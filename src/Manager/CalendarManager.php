<?php

namespace Tigris\CalendarBundle\Manager;

use App\Entity\Calendar\Resource;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Tigris\BaseBundle\Manager\AbstractManager;
use Tigris\CalendarBundle\Entity\Booking;
use Tigris\CalendarBundle\Repository\BookingCategoryRepository;
use Tigris\CalendarBundle\Repository\BookingRepository;
use Tigris\CalendarBundle\Repository\ResourceRepository;

class CalendarManager extends AbstractManager
{
    public function __construct(EntityManagerInterface $em, BookingCategoryRepository $bookingCategory, private readonly ResourceRepository $resourceRepository)
    {
        $this->setManager($em);
        $this->setRepository($bookingCategory);
    }

    public function findBookingCategories()
    {
        return $this->getRepository()->findBooking();
    }

    public function findResourceData(array $criteria): Paginator
    {
        return $this->resourceRepository->findData($criteria);
    }

    public function saveResource(Resource $entity): void
    {
        $em = $this->getManager();
        $em->persist($entity);
        $em->flush();
    }

    public function removeResource(Resource $entity): void
    {
        $em = $this->getManager();
        $em->remove($entity);
        $em->flush();
    }

    public function saveBooking(Booking $entity): void
    {
        $em = $this->getManager();
        $em->persist($entity);

        $em->flush();
    }
}
