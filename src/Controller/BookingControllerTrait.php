<?php

namespace Tigris\CalendarBundle\Controller;

use App\Entity\Calendar\Resource;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\NotificationEvent;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\BaseBundle\Repository\UserRepository;
use Tigris\BaseBundle\Utils\Utils;
use Tigris\CalendarBundle\Entity\Booking;
use Tigris\CalendarBundle\Entity\UserBookingInterface;
use Tigris\CalendarBundle\Form\Type\BookingType;
use Tigris\CalendarBundle\Manager\CalendarManager;
use Tigris\CalendarBundle\Repository\BookingCategoryRepository;
use Tigris\CalendarBundle\Repository\BookingRepository;
use Tigris\CalendarBundle\Repository\ResourceRepository;
use Tigris\CalendarBundle\Security\Voter\BookingVoter;

trait BookingControllerTrait
{
    #[Route(path: '/')]
    public function index(BookingCategoryRepository $categoryRepository, ConfigManager $configManager): Response
    {
        $this->generateBreadcrumbs();

        $user = $this->getUser();

        $defaultSelectedResource = null;
        $defaultSelectedCategory = null;

        if ($user instanceof UserBookingInterface) {
            $defaultSelectedResource = $user->getDefaultResource()?->getId();
            $defaultSelectedCategory = $user->getDefaultResource() instanceof Resource ? $user->getDefaultResource()->getCategory()?->getId() : null;
        }

        $categories = $categoryRepository->findParentForUser($user);
        $activeDays = $configManager->getValue('TigrisCalendarBundle.booking_active_days', [0, 1, 2, 3, 4, 5, 6]);

        $parameters = $this->getParameter('tigris_calendar');
        $allowAdd = $parameters['allow_user_booking'];

        if ($this->isGranted('ROLE_ADMIN')) {
            $activeDays = [0, 1, 2, 3, 4, 5, 6];
        }

        return $this->render('@TigrisCalendar/'.$this->viewPrefix.'booking/index.html.twig', [
            'categories' => $categories,
            'activeDays' => $activeDays,
            'allowAdd' => $allowAdd,
            'defaultSelectedResource' => $defaultSelectedResource,
            'defaultSelectedCategory' => $defaultSelectedCategory,
        ]);
    }

    #[Route(path: '/calendar-items.json', options: ['expose' => true], methods: ['GET'])]
    public function calendarItems(Request $request, BookingRepository $bookingRepository, ResourceRepository $resourceRepository): JsonResponse
    {
        $start = $request->get('start');
        $end = $request->get('end');
        $criteria = $this->getCriteria($request);
        $criteria['start'] = new \DateTime($start);
        $criteria['end'] = new \DateTime($end);
        $criteria['resources'] = empty($request->get('resources')) ? false : explode(',', $request->get('resources'));
        if ($criteria['resources']) {
            $resources = $resourceRepository->findByIds($criteria['resources']);
        } else {
            $resources = $resourceRepository->findBy(['status' => Resource::STATUS_AVAILABLE]);
        }

        $events = [];

        foreach ($resources as $resource) {
            foreach ($resource->getLockTimes() as $lockTime) {
                $lockDateTimes = $lockTime->getLockTimeDateTimes($criteria['start'], $criteria['end']);
                foreach ($lockDateTimes as $lockDateTime) {
                    $events[] = [
                        'id' => null,
                        'title' => $resource->getName(),
                        'start' => $lockDateTime['start']->format('Y-m-d H:i:s'),
                        'end' => $lockDateTime['end']->format('Y-m-d H:i:s'),
                        'backgroundColor' => '#ee62629e',
                        'borderColor' => '#ee62629e',
                        'allDay' => false,
                        'classNames' => ['lock-time'],
                    ];
                }
            }
        }

        $data = $bookingRepository->findData($criteria);
        $dataArray = $data->getIterator();

        if ($dataArray instanceof \ArrayIterator) {
            foreach ($dataArray->getArrayCopy() as $entity) {
                $events[] = [
                    'id' => $entity->getId(),
                    'title' => $entity->getResource()->getName(),
                    'start' => $entity->getStartDateTime()->format('Y-m-d H:i:s'),
                    'end' => $entity->getEndDateTime()->format('Y-m-d H:i:s'),
                    'backgroundColor' => $entity->getResource()->getCategory()->getColor(),
                    'borderColor' => $entity->getResource()->getCategory()->getColor(),
                    'allDay' => $entity->getAllDay(),
                ];
            }
        }

        return new JsonResponse($events);
    }

    #[Route(path: '/data', options: ['expose' => true], methods: ['GET'])]
    #[IsGranted('ROLE_ADMIN')]
    public function data(Request $request, BookingRepository $bookingRepository): JsonResponse
    {
        $start = $request->get('startDate', date('Y').'-01-01');
        $end = $request->get('endDate', date('Y').'-12-31');

        $criteria = $this->getCriteria($request);
        $criteria['start'] = \DateTime::createFromFormat('Y-m-d', Utils::convertJsDateTime($start, 'Y-m-d'));
        $criteria['end'] = \DateTime::createFromFormat('Y-m-d', Utils::convertJsDateTime($end, 'Y-m-d'));
        $criteria['userId'] = (int) $request->get('user', 0);
        $criteria['canceled'] = 'true' === $request->get('canceled', 'false');
        $resourceId = (int) $request->get('resource', 0);

        if (0 != $resourceId) {
            $criteria['resources'] = [$resourceId];
        }

        $data = $bookingRepository->findData($criteria);

        return $this->paginatorToJsonResponse($data);
    }

    #[Route(path: '/new/{id<\d+>}', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function new(Request $request, Resource $resource, UserRepository $userRepository, TranslatorInterface $translator, EntityManagerInterface $entityManager, EventDispatcherInterface $eventDispatcher): Response
    {
        $dateStartStr = $request->query->get('dateStart');
        $dateStart = new \DateTime($dateStartStr);
        $dateEnd = clone $dateStart;
        $dateEnd->modify('+1 hour');

        $entity = (new Booking())
            ->setUser($this->getUser())
            ->setOwner($this->getUser())
            ->setResource($resource)
            ->setStartDate($dateStart)
            ->setStartTime($dateStart)
            ->setEndDate($dateEnd)
            ->setEndTime($dateEnd)
        ;

        $form = $this->createForm(BookingType::class, $entity, [
            'method' => 'POST',
            'action' => $this->generateUrl('tigris_calendar_'.$this->routePrefix.'booking_new', ['id' => $resource->getId()]),
        ]);
        $this->addFormActions($form, $this->generateUrl('tigris_calendar_'.$this->routePrefix.'booking_index'));

        $form->handleRequest($request);

        if (!$this->isGranted(BookingVoter::CREATE, $entity)) {
            return $this->render('@TigrisCalendar/'.$this->viewPrefix.'booking/cannot_booking.html.twig', [
                'entity' => $entity,
                'reason' => BookingVoter::$reason,
            ]);
        }

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($entity);
            $entityManager->flush();

            if ($resource->isSendNotification()) {
                $admins = $userRepository->findAdmins();
                foreach ($admins as $admin) {
                    $event = new NotificationEvent($this->getUser(), $admin, [
                        'title' => 'calendar.booking_notification.title',
                        'message' => 'calendar.booking_notification.message',
                        'messageParams' => ['%name%' => $resource->getName()],
                        'buttons' => [
                            ['label' => 'calendar.booking_notification.btn', 'route' => 'tigris_calendar_'.$this->routePrefix.'booking_index', 'routeParams' => []],
                        ],
                    ], $resource);

                    $eventDispatcher->dispatch($event, Events::NOTIFY);
                }
            }

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse([
                    'status' => 'success',
                    'message' => $translator->trans('calendar.booking.add.success'),
                    'entity' => json_decode($this->getSerializer()->serialize($entity, 'json'), true, 512, JSON_THROW_ON_ERROR),
                ]);
            } else {
                $this->addFlash('success', 'calendar.booking.add.success');

                return $this->redirectToRoute('tigris_calendar_'.$this->routePrefix.'booking_index');
            }
        }

        return $this->render('@TigrisCalendar/'.$this->viewPrefix.'booking/new.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    #[Route(path: '/{id<\d+>}/edit', options: ['expose' => true], methods: ['GET', 'PUT'])]
    #[IsGranted('ROLE_USER')]
    public function edit(Request $request, Booking $entity, CalendarManager $calendarManager, TranslatorInterface $translator, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('edit', $entity);
        $form = $this->createForm(BookingType::class, $entity, ['method' => 'PUT', 'action' => $this->generateUrl('tigris_calendar_'.$this->routePrefix.'booking_edit', ['id' => $entity->getId()])]);
        $this->addFormActions($form, $this->generateUrl('tigris_calendar_'.$this->routePrefix.'booking_index'));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse([
                    'status' => 'success',
                    'message' => $translator->trans('calendar.booking.edit.success'),
                    'entity' => json_decode($this->getSerializer()->serialize($entity, 'json'), true, 512, JSON_THROW_ON_ERROR),
                ]);
            } else {
                $this->addFlash('success', 'calendar.booking.edit.success');

                return $this->redirectToRoute('tigris_calendar_'.$this->routePrefix.'booking_index');
            }
        }

        return $this->render('@TigrisCalendar/'.$this->viewPrefix.'booking/edit.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    #[Route(path: '/{id<\d+>}/cancel', options: ['expose' => true], methods: ['GET', 'PUT'])]
    public function cancel(Request $request, Booking $entity, TranslatorInterface $translator, EntityManagerInterface $entityManager): Response
    {
        $this->denyAccessUnlessGranted('edit', $entity);
        $form = $this->createFormBuilder($entity, [
            'method' => 'PUT',
            'action' => $this->generateUrl('tigris_calendar_'.$this->routePrefix.'booking_cancel', ['id' => $entity->getId()]),
        ])->getForm();

        $this->addFormActions($form, $this->generateUrl('tigris_calendar_'.$this->routePrefix.'booking_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entity->setCancelDate(new \DateTime());
            $entityManager->flush();

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse([
                    'status' => 'success',
                    'message' => $translator->trans('calendar.booking.cancel.success'),
                    'entity' => json_decode($this->getSerializer()->serialize($entity, 'json'), true, 512, JSON_THROW_ON_ERROR),
                ]);
            } else {
                $this->addFlash('success', 'calendar.booking.cancel.success');

                return $this->redirectToRoute('tigris_calendar_'.$this->routePrefix.'booking_index');
            }
        }

        return $this->render('@TigrisCalendar/'.$this->viewPrefix.'booking/cancel.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    #[Route(path: '/{id<\d+>}/show', options: ['expose' => true], methods: ['GET'])]
    public function show(Request $request, Booking $entity): Response
    {
        $this->denyAccessUnlessGranted(BookingVoter::VIEW, $entity);

        $entity->setCanCancel($this->isGranted(BookingVoter::CANCEL, $entity));
        $entity->setCanEdit($this->isGranted(BookingVoter::EDIT, $entity));

        if ($request->isXmlHttpRequest()) {
            return new JsonResponse([
                'status' => 'success',
                'entity' => json_decode($this->getSerializer()->serialize($entity, 'json'), true, 512, JSON_THROW_ON_ERROR),
            ]);
        }

        return $this->render('@TigrisCalendar/'.$this->viewPrefix.'booking/show.html.twig', [
            'entity' => $entity,
        ]);
    }
}
