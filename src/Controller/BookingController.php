<?php

namespace Tigris\CalendarBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\CalendarBundle\Entity\Model\ResourceModel;
use Tigris\CalendarBundle\Repository\BookingCategoryRepository;
use Tigris\CalendarBundle\Repository\BookingRepository;
use Tigris\CalendarBundle\Repository\ResourceRepository;

#[Route(path: 'booking')]
class BookingController extends BaseController
{
    use FormTrait;
    use BookingControllerTrait;
    private $routePrefix = '';
    
    private $viewPrefix = '';

    public function _previewWidget(Request $request, BookingRepository $bookingRepository, ResourceRepository $resourceRepository, array $excludeDays = []): Response
    {
        $year = $request->query->get('y', date('Y'));
        $month = $request->query->get('m', date('m'));
        $dayCount = cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y'));

        $bookings = $bookingRepository->findByMonth($month, $year);
        $resources = $resourceRepository->findBy(['visibility' => ResourceModel::VISIBILITY_ALL, 'status' => ResourceModel::STATUS_AVAILABLE]);

        // Generate array
        $days = [];
        for ($i = 1; $i <= $dayCount; ++$i) {
            $days[$i] = ['count' => 0];

            foreach ($resources as $resource) {
                $days[$i][$resource->getId()] = ['count' => 0, 'bookings' => [], 'name' => $resource->geTName()];
            }
        }

        foreach ($bookings as $booking) {
            $start = $booking->getStartDate();
            $end = $booking->getEndDate();
            $resource = $booking->getResource();

            $dayStart = $start->format('d');
            $dayEnd = $end->format('d');

            if ($start->format('m') != $end->format('m')) { // Pour les reservations à cheval sur deux mois
                if ($month == $start->format('m')) { // On consulte le premier mois
                    $dayEnd = $dayCount;
                } elseif ($month == $end->format('m')) { // On consulte le dernier mois
                    $dayStart = 1;
                } else {
                    $dayStart = 1;
                    $dayEnd = $dayCount;
                }
            }

            for ($i = (int) $dayStart; $i <= (int) $dayEnd; ++$i) {
                if (isset($days[$i][$resource->getId()])) {
                    ++$days[$i][$resource->getId()]['count'];
                    ++$days[$i]['count'];
                    $days[$i][$resource->getId()]['bookings'][] = $booking;
                }
            }
        }

        $prevMonth = (1 == $month) ? 12 : $month - 1;
        $nextMonth = (12 == $month) ? 1 : $month + 1;
        $prevYear = (1 == $month) ? $year - 1 : $year;
        $nextYear = (12 == $month) ? $year + 1 : $year;

        return $this->render('@TigrisCalendar/booking/_preview_widget.html.twig', [
            'nbDays' => $dayCount,
            'days' => $days,
            'resources' => $resources,
            'month' => $month,
            'year' => $year,
            'nextMonth' => $nextMonth,
            'prevMonth' => $prevMonth,
            'nextYear' => $nextYear,
            'prevYear' => $prevYear,
            'excludeDays' => $excludeDays,
        ]);
    }

    #[Route(path: '/list')]
    public function list(BookingCategoryRepository $bookingCategoryRepository): Response
    {
        $this->generateBreadcrumbs([
            'calendar.menu.booking_userlist' => ['route' => 'tigris_calendar_booking_list'],
        ]);
        $categories = $bookingCategoryRepository->findAll();
        $startDate = new \DateTime();
        $startDate->sub(new \DateInterval('P3M'));
        
        $endDate = new \DateTime();
        $endDate->add(new \DateInterval('P3M'));

        return $this->render('@TigrisCalendar/booking/list.html.twig', [
            'categories' => $categories,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);
    }

    #[Route(path: '/user-data', options: ['expose' => true])]
    public function userData(Request $request, BookingRepository $bookingRepository): Response
    {
        $user = $this->getUser();

        if (!$user instanceof User) {
            throw new AccessDeniedException();
        }

        $request->query->set('user', $user->getId());

        return $this->data($request, $bookingRepository);
    }
}
