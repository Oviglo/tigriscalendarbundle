<?php

namespace Tigris\CalendarBundle\Controller;

use App\Entity\Calendar\Event;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Tigris\BaseBundle\Controller\BaseController;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\CalendarBundle\Form\Type\EventType;
use Tigris\CalendarBundle\Form\Type\NewEventType;
use Tigris\CalendarBundle\Repository\EventRepository;

#[Route(path: '/calendar/event')]
class EventController extends BaseController
{
    use FormTrait;

    public function generateBreadcrumbs(array $routes = []): void
    {
        $routes = array_merge([
            'calendar.menu.events' => ['route' => 'tigris_calendar_event_index'],
        ], $routes);

        parent::generateBreadcrumbs($routes);
    }

    #[Route(path: '/', methods: ['GET'])]
    public function index(): Response
    {
        $this->generateBreadcrumbs();

        return $this->render('@TigrisCalendar/event/index.html.twig');
    }

    #[Route(path: '/list', options: ['expose' => true], methods: ['GET'])]
    public function list(Request $request, EventRepository $eventRepository): JsonResponse
    {
        $start = $request->get('start');
        $end = $request->get('end');
        $criteria = $this->getCriteria($request);
        $criteria['start'] = new \DateTime($start);
        $criteria['end'] = new \DateTime($end);
        $data = $eventRepository->findData($criteria);

        $events = [];
        foreach ($data->getIterator()->getArrayCopy() as $entity) {
            $className = '';
            foreach ($entity->getCategories() as $category) {
                $className .= $category->getSlug().' ';
            }
            
            $events[] = [
                'id' => $entity->getId(),
                'title' => $entity->getName(),
                'className' => $className,
                'start' => $entity->getStartDateTime()->format('Y-m-d H:i:s'),
                'end' => $entity->getEndDateTime()->format('Y-m-d H:i:s'),
                'allDay' => $entity->getAllDay(),
            ];
        }

        return new JsonResponse($events);
    }

    #[Route(path: '/new', options: ['expose' => true], methods: ['GET', 'POST'])]
    #[IsGranted('ROLE_USER')]
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $dateStartStr = $request->query->get('dateStart');
        $dateStart = new \DateTime($dateStartStr);
        $entity = (new Event())
            ->setStart($dateStart)
        ;

        $form = $this->createForm(NewEventType::class, $entity, ['method' => 'POST', 'action' => $this->generateUrl('tigris_calendar_event_new'), 'attr' => ['no-ajax' => true]]);
        $this->addFormActions($form);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($entity);
            $entityManager->flush();

            return $this->redirectToRoute('tigris_calendar_event_edit', ['id' => $entity->getId()]);
        }

        return $this->render('@TigrisCalendar/event/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route(path: '/{id}/edit', options: ['expose' => true], methods: ['GET', 'PUT'], requirements: ['id' => '\d+'])]
    #[IsGranted('ROLE_USER')]
    public function edit(Request $request, Event $entity, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(EventType::class, $entity, [
            'method' => 'PUT',
            'action' => $this->generateUrl('tigris_calendar_event_edit', ['id' => $entity->getId()]),
        ]);

        $this->addFormActions($form);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$entity->getActiveRepeat()) {
                $entity->setRepeat(null);
            }
            
            $entityManager->flush();
        }

        return $this->render('@TigrisCalendar/event/edit.html.twig', [
            'form' => $form,
            'entity' => $entity,
        ]);
    }

    #[Route(path: '/{id}/preview', options: ['expose' => true], requirements: ['id' => '\d+'])]
    public function preview(Event $entity): Response
    {
        return $this->render('@TigrisCalendar/event/preview.html.twig', ['entity' => $entity]);
    }

    #[Route(path: '/{slug}', options: ['expose' => true], requirements: ['slug' => '[a-zA-Z0-9\-_]+'])]
    public function show(Event $entity): Response
    {
        $this->generateBreadcrumbs([
            $entity->getName() => ['route' => 'tigris_calendar_event_show', 'params' => ['slug' => $entity->getSlug()]],
        ]);

        return $this->render('@TigrisCalendar/event/show.html.twig', ['entity' => $entity]);
    }

    public function next(EventRepository $eventRepository, int $count = 5): Response
    {
        $entities = $eventRepository->findNext($count);

        return $this->render('@TigrisCalendar/event/_next.html.twig', ['entities' => $entities]);
    }
}
