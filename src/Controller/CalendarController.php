<?php

namespace Tigris\CalendarBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Tigris\BaseBundle\Controller\BaseController;

#[Route(path: '/calendar')]
class CalendarController extends BaseController
{
}
