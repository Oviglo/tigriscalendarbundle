<?php

namespace Tigris\CalendarBundle\Controller\Admin;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Tigris\BaseBundle\Controller\Admin\BaseController;

/**
 * @IsGranted("ROLE_ADMIN")
 */
#[Route(path: 'calendar/admin/data')]
class DataController extends BaseController
{
}
