<?php

namespace Tigris\CalendarBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\CalendarBundle\Entity\BookingCategory;
use Tigris\CalendarBundle\Form\Type\BookingCategoryType;
use Tigris\CalendarBundle\Repository\BookingCategoryRepository;

#[Route(path: '/admin/calendar/booking/category')]
#[IsGranted('ROLE_ADMIN')]
class BookingCategoryController extends BaseController
{
    use FormTrait;

    public function generateBreadcrumbs(array $routes = []): void
    {
        $routes = array_merge([
            'calendar.menu.booking' => null,
            'calendar.menu.categories' => ['route' => 'tigris_calendar_admin_bookingcategory_index'],
        ], $routes);

        parent::generateBreadcrumbs($routes);
    }

    #[Route(path: '/', methods: ['GET'])]
    public function index(): Response
    {
        $this->generateBreadcrumbs();

        return $this->render('@TigrisCalendar/admin/booking_category/index.html.twig', []);
    }

    #[Route(path: '/data', options: ['expose' => 'admin'], methods: ['GET'])]
    public function data(Request $request, BookingCategoryRepository $categoryRepository): JsonResponse
    {
        $data = $categoryRepository->findData($this->getCriteria($request));

        return $this->paginatorToJsonResponse($data);
    }

    #[Route(path: '/new', options: ['expose' => 'admin'], methods: ['GET', 'POST'])]
    public function new(Request $request, TranslatorInterface $translator, EntityManagerInterface $entityManager): Response
    {
        $entity = new BookingCategory();
        $form = $this->createForm(BookingCategoryType::class, $entity, [
            'method' => 'POST',
            'action' => $this->generateUrl('tigris_calendar_admin_bookingcategory_new'),
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_calendar_admin_bookingcategory_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($entity);
            $entityManager->flush();
            if ($request->isXmlHttpRequest()) {
                return new Jsonresponse(['status' => 'success', 'message' => $translator->trans('calendar.category.add.success')]);
            } else {
                $this->addFlash('success', 'calendar.category.add.success');

                return $this->redirectToRoute('tigris_calendar_admin_bookingcategory_index');
            }
        }

        return $this->render('@TigrisCalendar/admin/booking_category/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route(path: '/edit/{id}', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['GET', 'PUT'])]
    public function edit(BookingCategory $entity, Request $request, TranslatorInterface $translator, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(BookingCategoryType::class, $entity, [
            'method' => 'PUT',
            'action' => $this->generateUrl('tigris_calendar_admin_bookingcategory_edit', ['id' => $entity->getId()]),
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_calendar_admin_bookingcategory_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            if ($request->isXmlHttpRequest()) {
                return new Jsonresponse(['status' => 'success', 'message' => $translator->trans('calendar.category.edit.success')]);
            } else {
                $this->addFlash('success', 'calendar.category.edit.success');

                return $this->redirectToRoute('tigris_calendar_admin_bookingcategory_index');
            }
        }

        return $this->render('@TigrisCalendar/admin/booking_category/edit.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route(path: '/remove/{id}', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['GET', 'DELETE'])]
    public function remove(BookingCategory $entity, Request $request, TranslatorInterface $translator, EntityManagerInterface $entityManager): Response
    {
        $form = $this->getDeleteForm($entity, 'tigris_calendar_admin_bookingcategory_');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->remove($entity);
            $entityManager->flush();
            if ($request->isXmlHttpRequest()) {
                return new Jsonresponse(['status' => 'success', 'message' => $translator->trans('calendar.category.remove.success')]);
            } else {
                $this->addFlash('success', 'calendar.category.remove.success');

                return $this->redirectToRoute('tigris_calendar_admin_bookingcategory_index');
            }
        }

        return $this->render('@TigrisCalendar/admin/booking_category/remove.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }

    #[Route(path: '/sort', options: ['expose' => 'admin'], methods: ['GET', 'PUT'])]
    public function sort(Request $request, BookingCategoryRepository $categoryRepository, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $form = $this
            ->createFormBuilder(null, [
                'attr' => [
                    'novalidate' => 'novalidate',
                ],
            ])
            ->setMethod(Request::METHOD_PUT)
            ->setAction($this->generateUrl('tigris_calendar_admin_bookingcategory_sort'))
            ->getForm()
        ;

        $this->addFormActions($form, $this->generateUrl('tigris_calendar_admin_bookingcategory_index'));

        $form->handleRequest($request);

        $entities = $categoryRepository->getTree();

        if ($form->isSubmitted() && $form->isValid()) {
            $allEntities = $categoryRepository->findAll();
            $positions = $request->request->all('position');
            foreach ($positions as $id => $position) {
                foreach ($allEntities as $entity) {
                    if ($entity->getId() === $id) {
                        $entity->setPosition($position);
                        $em->persist($entity);

                        break;
                    }
                }
            }

            $em->flush();

            // reorder
            $categoryRepository->reorderAll('position');

            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('calendar.category.sort.success')]);
            } else {
                $this->addFlash('success', 'calendar.category.sort.success');

                return $this->redirectToRoute('tigris_calendar_admin_bookingcategory_index');
            }
        }

        return $this->render('@TigrisCalendar\admin\booking_category\sort.html.twig', [
            'form' => $form,
            'entities' => $entities,
        ]);
    }
}
