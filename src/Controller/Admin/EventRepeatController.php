<?php

namespace Tigris\CalendarBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\Admin\AbstractCrudController;
use Tigris\BaseBundle\Crud\Crud;
use Tigris\CalendarBundle\Entity\EventRepeat;
use Tigris\CalendarBundle\Form\Type\EventRepeatType;
use Tigris\CalendarBundle\Repository\EventRepeatRepository;

/**
 * @IsGranted("ROLE_ADMIN")
 */
#[Route(path: 'calendar/admin/repeat')]
class EventRepeatController extends AbstractCrudController
{
    protected function generateCrud(): Crud
    {
        return new Crud([
            Crud::ENTITY_CLASS => EventRepeat::class,
            Crud::FORM_CLASS => EventRepeatType::class,
            Crud::ROUTE_PREFIX => 'tigris_calendar_admin_eventrepeat',
            Crud::REPOSITORY => $this->eventRepeatRepository,
        ]);
    }

    public function __construct(private readonly EventRepeatRepository $eventRepeatRepository, EntityManagerInterface $em, TranslatorInterface $translator)
    {
        parent::__construct($em, $translator);
    }

    public function generateBreadcrumbs(array $routes = []): void
    {
        $routes = array_merge([
            'calendar.menu.events' => null,
            'calendar.menu.repeats' => ['route' => 'tigris_calendar_admin_eventrepeat_index'],
        ], $routes);

        parent::generateBreadcrumbs($routes);
    }

    #[Route(path: '/')]
    public function index(): Response
    {
        return $this->indexCRUD(['iterations']);
    }

    #[Route(path: '/{id}/delete', requirements: ['id' => '\d+'], methods: ['GET', 'DELETE'], options: ['expose' => 'admin'])]
    public function delete(Request $request, EventRepeat $entity, TranslatorInterface $translator, EntityManagerInterface $em): Response
    {
        $this->generateBreadcrumbs();

        return $this->deleteCRUD($entity, $request, $translator->trans('event_repeat.delete.success'));
    }

    protected function generateViewPath($action): string
    {
        if ($action == 'index') {
            return '@TigrisCalendar\admin\event_repeat\\index.html.twig';
        }

        return '@TigrisBase\admin\crud\\'.$action.'.html.twig';
    }
}
