<?php

namespace Tigris\CalendarBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\CalendarBundle\Entity\EventCategory;
use Tigris\CalendarBundle\Form\Type\EventCategoryType;
use Tigris\CalendarBundle\Repository\EventCategoryRepository;

#[Route(path: '/admin/calendar/event/category')]
#[IsGranted('ROLE_ADMIN')]
class EventCategoryController extends BaseController
{
    use FormTrait;

    public function generateBreadcrumbs(array $routes = []): void
    {
        $routes = array_merge([
            'calendar.menu.events' => null,
            'calendar.menu.categories' => ['route' => 'tigris_calendar_admin_eventcategory_index'],
        ], $routes);

        parent::generateBreadcrumbs($routes);
    }

    #[Route(path: '/', methods: ['GET'])]
    public function index(): Response
    {
        $this->generateBreadcrumbs();

        return $this->render('@TigrisCalendar/admin/event_category/index.html.twig', []);
    }

    #[Route(path: '/list', options: ['expose' => 'admin'], methods: ['GET'])]
    public function list(Request $request, EventCategoryRepository $categoryRepository): JsonResponse
    {
        $data = $categoryRepository->findData($this->getCriteria($request));

        return $this->paginatorToJsonResponse($data);
    }

    #[Route(path: '/new', options: ['expose' => 'admin'], methods: ['GET', 'POST'])]
    public function new(Request $request, TranslatorInterface $translator, EntityManagerInterface $entityManager): Response
    {
        $entity = new EventCategory();
        $form = $this->createForm(EventCategoryType::class, $entity, [
            'action' => $this->generateUrl('tigris_calendar_admin_eventcategory_new'),
            'method' => 'POST',
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_calendar_admin_eventcategory_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->persist($entity);
            $entityManager->flush();
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('calendar.category.add.success')]);
            }
            $this->addFlash('success', 'calendar.category.add.success');

            return $this->redirectToRoute('tigris_calendar_admin_eventcategory_index');
        }

        return $this->render('@TigrisCalendar/admin/event_category/new.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route(path: '/edit/{id}', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['GET', 'POST'])]
    public function edit(EventCategory $entity, Request $request, TranslatorInterface $translator, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(EventCategoryType::class, $entity, [
            'action' => $this->generateUrl('tigris_calendar_admin_eventcategory_edit', ['id' => $entity->getId()]),
            'method' => 'POST',
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_calendar_admin_eventcategory_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->flush();
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('calendar.category.edit.success')]);
            }
            $this->addFlash('success', 'calendar.category.edit.success');

            return $this->redirectToRoute('tigris_calendar_admin_eventcategory_index');
        }

        return $this->render('@TigrisCalendar/admin/event_category/edit.html.twig', [
            'form' => $form,
        ]);
    }

    #[Route(path: '/remove/{id}', requirements: ['id' => '\d+'], options: ['expose' => 'admin'], methods: ['GET', 'DELETE'])]
    public function remove(EventCategory $entity, Request $request, TranslatorInterface $translator, EntityManagerInterface $entityManager): Response
    {
        $form = $this->getDeleteForm($entity, 'tigris_calendar_admin_eventcategory_');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager->remove($entity);
            $entityManager->flush();
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('calendar.category.remove.success')]);
            }
            $this->addFlash('success', 'calendar.category.remove.success');

            return $this->redirectToRoute('tigris_calendar_admin_eventcategory_index');
        }

        return $this->render('@TigrisCalendar/admin/event_category/remove.html.twig', [
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }
}
