<?php

namespace Tigris\CalendarBundle\Controller\Admin;

use App\Entity\Calendar\Resource;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\CalendarBundle\Form\Type\ResourceType;
use Tigris\CalendarBundle\Manager\CalendarManager;

#[Route(path: '/admin/calendar/resource')]
#[IsGranted('ROLE_ADMIN')]
class ResourceController extends BaseController
{
    use FormTrait;

    public function generateBreadcrumbs(array $routes = []): void
    {
        $routes = array_merge([
            'calendar.menu.booking' => null,
            'calendar.menu.resources' => ['route' => 'tigris_calendar_admin_resource_index'],
        ], $routes);

        parent::generateBreadcrumbs($routes);
    }

    #[Route(path: '/', methods: ['GET'])]
    public function index(): Response
    {
        $this->generateBreadcrumbs();

        return $this->render('@TigrisCalendar/admin/booking/resource/index.html.twig', []);
    }

    #[Route(path: '/list', options: ['expose' => 'admin'], methods: ['GET'])]
    public function list(Request $request, CalendarManager $calendarManager): JsonResponse
    {
        $data = $calendarManager->findResourceData($this->getCriteria($request));

        return $this->paginatorToJsonResponse($data);
    }

    #[Route(path: '/new', options: ['expose' => 'admin'], methods: ['GET', 'POST'])]
    public function new(Request $request, CalendarManager $calendarManager, TranslatorInterface $translator): Response
    {
        $this->generateBreadcrumbs([
            'calendar.resource.add.title' => ['route' => 'tigris_calendar_admin_resource_new'],
        ]);

        $parameters = $this->getParameter('tigris_calendar');
        $allowAdd = $parameters['allow_user_booking'];

        $entity = new Resource();
        $form = $this->createForm(ResourceType::class, $entity, [
            'action' => $this->generateUrl('tigris_calendar_admin_resource_new'),
            'method' => 'POST',
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_calendar_admin_resource_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $calendarManager->saveResource($entity);
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('calendar.resource.add.success')]);
            }

            $this->addFlash('success', 'calendar.resource.add.success');

            return $this->redirectToRoute('tigris_calendar_admin_resource_index');
        }

        return $this->render('@TigrisCalendar/admin/booking/resource/new.html.twig', [
            'form' => $form,
            'allowUserBooking' => $allowAdd,
        ]);
    }

    #[Route(path: '/edit/{id}', options: ['expose' => 'admin'], methods: ['GET', 'POST'])]
    public function edit(Request $request, Resource $entity, CalendarManager $calendarManager, TranslatorInterface $translator): Response
    {
        $this->generateBreadcrumbs([
            'calendar.resource.edit.title' => ['route' => 'tigris_calendar_admin_resource_edit', 'params' => ['id' => $entity->getId()]],
        ]);

        $parameters = $this->getParameter('tigris_calendar');
        $allowAdd = $parameters['allow_user_booking'];

        $form = $this->createForm(ResourceType::class, $entity, [
            'action' => $this->generateUrl('tigris_calendar_admin_resource_edit', ['id' => $entity->getId()]),
            'method' => 'POST',
        ]);

        $this->addFormActions($form, $this->generateUrl('tigris_calendar_admin_resource_index'));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $calendarManager->saveResource($entity);
            if ($request->isXmlHttpRequest()) {
                return new JsonResponse(['status' => 'success', 'message' => $translator->trans('calendar.resource.edit.success')]);
            }

            $this->addFlash('success', 'calendar.resource.add.success');

            return $this->redirectToRoute('tigris_calendar_admin_resource_index');
        }

        return $this->render('@TigrisCalendar/admin/booking/resource/edit.html.twig', [
            'form' => $form,
            'entity' => $entity,
            'allowUserBooking' => $allowAdd,
        ]);
    }
}
