<?php

namespace Tigris\CalendarBundle\Controller\Admin;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Tigris\BaseBundle\Controller\Admin\BaseController;
use Tigris\BaseBundle\Repository\UserRepository;
use Tigris\BaseBundle\Traits\FormTrait;
use Tigris\CalendarBundle\Controller\BookingControllerTrait;
use Tigris\CalendarBundle\Repository\BookingCategoryRepository;

#[IsGranted('ROLE_ADMIN')]
#[Route(path: '/calendar/admin/booking')]
class BookingController extends BaseController
{
    use FormTrait;
    use BookingControllerTrait;
    private $routePrefix = 'admin_';
    
    private $viewPrefix = 'admin/';

    public function generateBreadcrumbs(array $routes = []): void
    {
        $routes = array_merge([
            'calendar.menu.booking' => null,
            // 'calendar.menu.calendar' => ['route' => 'tigris_calendar_admin_booking_index'],
        ], $routes);

        parent::generateBreadcrumbs($routes);
    }

    #[Route(path: '/list')]
    public function list(BookingCategoryRepository $bookingCategoryRepository, UserRepository $userRepository): Response
    {
        $this->generateBreadcrumbs([
            'calendar.menu.booking_list' => ['route' => 'tigris_calendar_admin_booking_list'],
        ]);
        $categories = $bookingCategoryRepository->findAll();
        $users = $userRepository->findBy(['enabled' => true], ['username' => 'asc']);
        $startDate = new \DateTime();
        $startDate->sub(new \DateInterval('P3M'));
        
        $endDate = new \DateTime();
        $endDate->add(new \DateInterval('P3M'));

        return $this->render('@TigrisCalendar/admin/booking/list.html.twig', [
            'categories' => $categories,
            'users' => $users,
            'startDate' => $startDate,
            'endDate' => $endDate,
        ]);
    }
}
