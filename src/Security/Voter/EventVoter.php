<?php

namespace Tigris\CalendarBundle\Security\Voter;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\CalendarBundle\Entity\Model\EventModel as Event;

class EventVoter extends Voter
{
    final public const CREATE = 'create';
    
    final public const EDIT = 'edit';

    public function __construct(private readonly Security $security)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        return $subject instanceof Event && in_array($attribute, [self::CREATE, self::EDIT]);
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::CREATE => $this->canCreate($subject, $user),
            self::EDIT => $this->canEdit($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    public function canCreate(Event $subject, $user): bool
    {
        return true;
    }

    public function canEdit(Event $subject, $user): bool
    {
        if ($user instanceof UserInterface) {
            return $this->security->isGranted('ROLE_ADMIN');
        }

        return false;
    }
}
