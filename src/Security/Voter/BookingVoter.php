<?php

namespace Tigris\CalendarBundle\Security\Voter;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Tigris\BaseBundle\Manager\ConfigManager;
use Tigris\CalendarBundle\Entity\Booking;
use Tigris\CalendarBundle\Repository\BookingRepository;

class BookingVoter extends Voter
{
    final public const CREATE = 'create';

    final public const EDIT = 'edit';

    final public const CANCEL = 'cancel';

    final public const VIEW = 'view';

    public static $reason = '';

    final public const REASON_LOWER_THAN_MIN_TIME = 'lower_than_min_time';

    final public const REASON_GREATER_THAN_MAX_TIME = 'greater_than_max_time';

    final public const REASON_ALLREADY_BOOKED = 'allready_booked';

    public function __construct(private readonly ConfigManager $configManager, private readonly AuthorizationCheckerInterface $authorisationChecker, private readonly BookingRepository $bookingRepository)
    {
    }

    protected function supports(string $attribute, $subject): bool
    {
        return $subject instanceof Booking && in_array($attribute, [self::CREATE, self::EDIT, self::CANCEL, self::VIEW]);
    }

    protected function voteOnAttribute(string $attribute, $subject, TokenInterface $token): bool
    {
        $user = $token->getUser();

        return match ($attribute) {
            self::CREATE => $this->canCreate($subject, $user),
            self::EDIT => $this->canEdit($subject, $user),
            self::CANCEL => $this->canCancel($subject),
            self::VIEW => $this->canView($subject, $user),
            default => throw new \LogicException('This code should not be reached!'),
        };
    }

    public function canView(Booking $subject, $user): bool
    {
        return true;
    }

    public function canCreate(Booking $subject, $user): bool
    {
        $dateStart = $subject->getStartDateTime();
        $dateEnd = $subject->getEndDateTime();

        $configMinTime = $this->configManager->getValue('TigrisCalendarBundle.booking_min_time', null);
        $configMaxTime = $this->configManager->getValue('TigrisCalendarBundle.booking_max_time', null);
        $configStartDate = $this->configManager->getValue('TigrisCalendarBundle.booking_start', null);
        $configEndDate = $this->configManager->getValue('TigrisCalendarBundle.booking_end', null);

        if ($this->authorisationChecker->isGranted('ROLE_ADMIN')) {
            return true;
        }

        // Test if allready booked
        if ($this->bookingRepository->isBooked($subject)) {
            self::$reason = self::REASON_ALLREADY_BOOKED;

            return false;
        }

        if (null !== $configMinTime && (int) $dateStart->format('G') < (int) $configMinTime->format('G')) { // Before min hour
            self::$reason = self::REASON_LOWER_THAN_MIN_TIME;

            return false;
        } elseif (null !== $configMinTime && (int) $dateStart->format('G') === (int) $configMinTime->format('G') && (int) $dateStart->format('i') < (int) $configMinTime->format('i')) {
            self::$reason = self::REASON_LOWER_THAN_MIN_TIME;

            return false;
        }

        if (null !== $configMaxTime && (int) $dateEnd->format('G') > (int) $configMaxTime->format('G')) { // After max hour
            self::$reason = self::REASON_GREATER_THAN_MAX_TIME;

            return false;
        } elseif (null !== $configMaxTime && (int) $dateEnd->format('G') === (int) $configMaxTime->format('G') && (int) $dateEnd->format('i') > (int) $configMaxTime->format('i')) {
            self::$reason = self::REASON_GREATER_THAN_MAX_TIME;

            return false;
        }

        if (null !== $configStartDate && $dateStart < $configStartDate) { // Min date
            self::$reason = 'min_date';

            return false;
        }

        if (null !== $configEndDate && $dateEnd > $configEndDate) { // Max date
            self::$reason = 'max_date';

            return false;
        }

        // Booking delay
        $bookingDelay = $this->configManager->getValue('TigrisCalendarBundle.booking_delay', 0);
        if ($bookingDelay > 10) {
            $bookingDelayDate = new \DateTime();
            $bookingDelayDate->modify(sprintf('+%s minutes', $bookingDelay));
            if ($dateStart < $bookingDelayDate) {
                self::$reason = 'delay';

                return false;
            }
        }

        // Lock times
        foreach ($subject->getResource()->getLockTimes() as $lockTime) {
            $bookingDay = (int) $dateStart->format('N');
            if (in_array($bookingDay, $lockTime->getDays())) {
                // Test hours
                $lockStartDateTime = clone $dateStart;
                $lockEndDateTime = clone $dateEnd;
                $lockStartDateTime->setTime((int) $lockTime->getStartTime()->format('G'), (int) $lockTime->getStartTime()->format('i'));
                $lockEndDateTime->setTime((int) $lockTime->getEndTime()->format('G'), (int) $lockTime->getEndTime()->format('i'));

                if ($dateStart > $lockStartDateTime && $dateStart < $lockEndDateTime) {
                    self::$reason = 'lock';

                    return false;
                }

                if ($dateEnd > $lockStartDateTime && $dateEnd < $lockEndDateTime) {
                    self::$reason = 'lock_2';

                    return false;
                }
            }
        }

        return true;
    }

    public function canEdit(Booking $subject, $user): bool
    {
        if ($this->authorisationChecker->isGranted('ROLE_ADMIN')) {
            return true;
        }

        return $subject->getOwner()->getId() == $user->getId();
    }

    public function canCancel(Booking $subject): bool
    {
        if ($this->authorisationChecker->isGranted('ROLE_ADMIN')) {
            return true;
        }

        $deleteDelay = $this->configManager->getValue('TigrisCalendarBundle.booking_delete_delay', null);

        if (null !== $deleteDelay) {
            $startDate = clone $subject->getStartDateTime();
            $startDate->modify(sprintf('-%s minutes', $deleteDelay));

            $currentDate = new \DateTime('now');

            if ($currentDate > $startDate) {
                return false;
            }
        }

        return true;
    }
}
