<?php

namespace Tigris\CalendarBundle\DataExporter;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\BaseBundle\DataExporter\AbstractExporter;
use Tigris\CalendarBundle\Repository\BookingRepository;

class BookingExporter extends AbstractExporter
{
    public function __construct(EntityManagerInterface $entityManager, array $configs, private readonly BookingRepository $bookingRepository, TranslatorInterface $translator)
    {
        parent::__construct($entityManager, $configs, $translator);
    }

    public function format(array $criteria = [], string $writerName = null): array
    {
        if (isset($criteria['endDate'])) {
            $criteria['endDate'] = new \DateTime($criteria['endDate']);
        }
        
        if (isset($criteria['startDate'])) {
            $criteria['startDate'] = new \DateTime($criteria['startDate']);
        }
        
        $entities = $this->bookingRepository->findData($criteria);

        $data = [];
        $data[] = $this->generateHeader(['user.username', 'calendar.booking.start', 'calendar.booking.duration.label', 'calendar.resource.resource', 'calendar.booking.canceled']);

        foreach ($entities as $entity) {
            $data[] = $this->generateRow(['user', 'startDate', 'formattedDuration', 'resource', 'canceled'], $entity);
        }

        return $data;
    }
}
