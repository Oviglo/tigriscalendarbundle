<?php

namespace Tigris\CalendarBundle\Entity;

use App\Entity\Calendar\Resource;
use Doctrine\ORM\Mapping as ORM;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\CalendarBundle\Repository\BookingRepository;

#[ORM\Entity(repositoryClass: BookingRepository::class)]
#[ORM\Table(name: 'calendar_booking')]
class Booking extends Item
{
    #[ORM\ManyToOne(targetEntity: Resource::class, inversedBy: 'bookings')]
    private Resource $resource;

    #[ORM\ManyToOne]
    private User $owner;

    #[ORM\ManyToOne(inversedBy: 'bookings')]
    private User $user;

    private bool $canCancel = false;
    private bool $canEdit = false;

    public function getResource(): Resource
    {
        return $this->resource;
    }

    public function setResource(Resource $resource): self
    {
        $this->resource = $resource;
        $this->name = $resource->getName();

        return $this;
    }

    public function getName(): string
    {
        return $this->resource->getName();
    }

    public function getOwner(): ?User
    {
        return $this->owner;
    }

    public function setOwner(User $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function canEdit(): bool
    {
        return $this->canEdit;
    }

    public function setCanEdit(bool $canEdit): self
    {
        $this->canEdit = $canEdit;

        return $this;
    }

    public function canCancel(): bool
    {
        return $this->canCancel;
    }

    public function setCanCancel(bool $canCancel): self
    {
        $this->canCancel = $canCancel;

        return $this;
    }
}
