<?php

namespace Tigris\CalendarBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\SoftDeleteable\Traits\SoftDeleteableEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\BaseBundle\Entity\File;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\CalendarBundle\Entity\Booking;
use Tigris\CalendarBundle\Entity\BookingCategory;
use Tigris\CalendarBundle\Entity\ResourceLockTime;
use Tigris\CalendarBundle\Repository\ResourceRepository;

#[ORM\MappedSuperclass(repositoryClass: ResourceRepository::class)]
class ResourceModel implements \Stringable
{
    use SoftDeleteableEntity;
    use TimestampableEntity;

    public const STATUS_AVAILABLE = 'available';

    public const STATUS_UNAVAILABLE = 'unavailable';

    public const VISIBILITY_ALL = 1;

    public const VISIBILITY_ADMINS = 2;

    public const VISIBILITY_SELECTED_USERS = 3;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected ?int $id = null;

    #[ORM\Column(length: 180)]
    protected string $name;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    protected ?string $description = null;

    #[ORM\Column(length: 50)]
    protected string $status = self::STATUS_AVAILABLE;

    #[ORM\Column(nullable: true)]
    protected ?string $statusMessage = null;

    #[ORM\ManyToOne(inversedBy: 'resources')]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    protected ?BookingCategory $category = null;

    #[ORM\OneToMany(targetEntity: ResourceLockTime::class, mappedBy: 'resource', cascade: ['all'], orphanRemoval: true)]
    #[Assert\Valid]
    protected Collection $lockTimes;

    #[ORM\Column(nullable: true)]
    #[Assert\GreaterThan(0)]
    protected ?int $maxDuration = null;

    #[ORM\Column(nullable: true)]
    #[Assert\GreaterThan(0)]
    protected ?int $minDuration = null;

    #[ORM\Column]
    protected int $visibility = self::VISIBILITY_ALL;

    #[ORM\ManyToMany(targetEntity: User::class)]
    protected Collection $selectedUsers;

    #[ORM\ManyToOne]
    protected ?File $image = null;

    #[ORM\OneToMany(targetEntity: Booking::class, mappedBy: 'resource')]
    #[JMS\Exclude]
    protected Collection $bookings;

    #[ORM\Column]
    protected bool $severalDays = false;

    #[ORM\Column]
    protected bool $sendNotification = false;

    public static function getStatusList(): array
    {
        return [
            self::STATUS_AVAILABLE => true,
            self::STATUS_UNAVAILABLE => false,
        ];
    }

    public function __construct()
    {
        $this->lockTimes = new ArrayCollection();
        $this->selectedUsers = new ArrayCollection();
        $this->bookings = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * Get the value of id.
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get the value of status.
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * Set the value of status.
     */
    public function setStatus(string $status): ResourceModel
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get the value of category.
     */
    public function getCategory(): ?BookingCategory
    {
        return $this->category;
    }

    /**
     * Set the value of category.
     */
    public function setCategory(?BookingCategory $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of description.
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * Set the value of description.
     */
    public function setDescription(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    public function getLockTimes(): Collection
    {
        return $this->lockTimes;
    }

    public function setLockTimes(Collection $lockTimes): self
    {
        $this->lockTimes->clear();
        foreach ($lockTimes as $lockTime) {
            $lockTime->setResource($this);
            $this->lockTimes[] = $lockTime;
        }

        return $this;
    }

    public function addLockTime(ResourceLockTime $lockTime): self
    {
        if (!$this->lockTimes->contains($lockTime)) {
            $lockTime->setResource($this);
            $this->lockTimes[] = $lockTime;
        }

        return $this;
    }

    public function getStatusMessage(): ?string
    {
        return $this->statusMessage;
    }

    public function setStatusMessage(?string $statusMessage): static
    {
        $this->statusMessage = $statusMessage;

        return $this;
    }

    public function getMaxDuration(): ?int
    {
        return $this->maxDuration;
    }

    public function setMaxDuration(?int $maxDuration): static
    {
        $this->maxDuration = $maxDuration;

        return $this;
    }

    public function getVisibility(): int
    {
        return $this->visibility;
    }

    public function setVisibility(int $visibility): static
    {
        $this->visibility = $visibility;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getSelectedUsers(): Collection
    {
        return $this->selectedUsers;
    }

    public function setSelectedUsers(ArrayCollection $selectedUsers): static
    {
        $this->selectedUsers = $selectedUsers;

        return $this;
    }

    public function addSelectedUser(User $user): static
    {
        if (!$this->selectedUsers->contains($user)) {
            $this->selectedUsers[] = $user;
        }

        return $this;
    }

    public function getImage(): ?File
    {
        return $this->image;
    }

    public function setImage(File $image): static
    {
        $this->image = $image;

        return $this;
    }

    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function setBookings(Collection $bookings): static
    {
        $this->bookings = $bookings;

        return $this;
    }

    /**
     * Get the value of minDuration.
     */
    public function getMinDuration(): ?int
    {
        return $this->minDuration;
    }

    /**
     * Set the value of minDuration.
     */
    public function setMinDuration(?int $minDuration): static
    {
        $this->minDuration = $minDuration;

        return $this;
    }

    public function getSeveralDays(): bool
    {
        return $this->severalDays;
    }

    public function setSeveralDays(bool $severalDays): static
    {
        $this->severalDays = $severalDays;

        return $this;
    }

    public function isSendNotification(): bool
    {
        return $this->sendNotification ?? false;
    }

    public function setSendNotification(bool $sendNotification): self
    {
        $this->sendNotification = $sendNotification;

        return $this;
    }
}
