<?php

namespace Tigris\CalendarBundle\Entity\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\CalendarBundle\Entity\EventCategory;
use Tigris\CalendarBundle\Entity\EventRepeat;
use Tigris\CalendarBundle\Entity\Item;
use Tigris\CalendarBundle\Repository\EventRepository;

#[ORM\MappedSuperclass(repositoryClass: EventRepository::class)]
#[ORM\HasLifecycleCallbacks]
class EventModel extends Item
{
    #[Gedmo\Slug(fields: ['name'], updatable: true)]
    #[ORM\Column(length: 150, unique: true)]
    protected string $slug;

    #[ORM\ManyToOne(cascade: ['all'], inversedBy: 'events')]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    protected EventRepeat|null $repeat;

    #[ORM\Column]
    protected bool $activeRepeat = false;

    #[ORM\ManyToMany(targetEntity: EventCategory::class)]
    #[ORM\JoinTable(name: 'calendar_event_event_category')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    #[ORM\InverseJoinColumn(onDelete: 'CASCADE', unique: true)]
    protected Collection $categories;

    #[ORM\ManyToOne]
    private User $user;

    public function __construct()
    {
        parent::__construct();
        $this->repeat = new EventRepeat();
        $this->repeat->setEvent($this);
        
        $this->categories = new ArrayCollection();
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getRepeat(): ?EventRepeat
    {
        return $this->repeat;
    }

    
    public function setRepeat(?EventRepeat $repeat): static
    {
        $this->repeat = $repeat;

        return $this;
    }

    public function getActiveRepeat(): bool
    {
        return $this->activeRepeat;
    }

    public function setActiveRepeat(bool $activeRepeat): static
    {
        $this->activeRepeat = $activeRepeat;

        return $this;
    }

    #[ORM\PrePersist]
    #[ORM\PreUpdate]
    public function prePersist(): void
    {
        if (!$this->activeRepeat) { // Don't save repeat if disabled
            $this->repeat = null;
        }
    }

    public function __clone()
    {
        $this->id = 0;
        $this->slug = null;
    }

    /**
     * Get the value of categories.
     *
     * @return ArrayCollection
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    /**
     * Set the value of categories.
     */
    public function setCategories(ArrayCollection $categories): static
    {
        $this->categories = $categories;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): static
    {
        $this->user = $user;

        return $this;
    }
}
