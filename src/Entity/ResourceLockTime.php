<?php

namespace Tigris\CalendarBundle\Entity;

use App\Entity\Calendar\Resource;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\CalendarBundle\Entity\Model\ResourceModel;

#[ORM\Entity]
#[ORM\Table(name: 'calendar_resource_lock_time')]
class ResourceLockTime
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(type: Types::JSON)]
    private array $days = [];

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private \DateTime $startTime;

    #[ORM\Column(type: Types::TIME_MUTABLE)]
    private \DateTime $endTime;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    private ?string $description = null;

    #[ORM\ManyToOne(targetEntity: Resource::class, inversedBy: 'lockTimes')]
    #[ORM\JoinColumn(nullable: true)]
    #[Assert\NotNull]
    private ResourceModel $resource;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDays(): array
    {
        return $this->days;
    }

    public function setDays(array $days): self
    {
        $this->days = $days;

        return $this;
    }

    public function getStartTime(): \DateTime
    {
        return $this->startTime;
    }

    public function setStartTime(\DateTime $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): \DateTime
    {
        return $this->endTime;
    }

    public function setEndTime(\DateTime $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getResource(): ResourceModel
    {
        return $this->resource;
    }

    public function setResource(ResourceModel $resource): self
    {
        $this->resource = $resource;

        return $this;
    }

    public function getStartDateTime(\DateTime $date): \DateTime
    {
        $returnDate = new \DateTime();
        $returnDate->setTimestamp(mktime($this->startTime->format('G'), $this->startTime->format('i'), 0, $date->format('n'), $date->format('j'), $date->format('Y')));

        return $returnDate;
    }

    public function getEndDateTime(\DateTime $date): \DateTime
    {
        $returnDate = new \DateTime();
        $returnDate->setTimestamp(mktime($this->endTime->format('G'), $this->endTime->format('i'), 0, $date->format('n'), $date->format('j'), $date->format('Y')));

        return $returnDate;
    }

    public function getLockTimeDateTimes(\DateTime $begin, \DateTime $end): array
    {
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($begin, $interval, $end);

        $dates = [];

        foreach ($period as $dt) {
            if (in_array($dt->format('N'), $this->days)) {
                $dates[] = ['start' => $this->getStartDateTime($dt), 'end' => $this->getEndDateTime($dt)];
            }
        }

        return $dates;
    }
}
