<?php

namespace Tigris\CalendarBundle\Entity;

use App\Entity\Calendar\Resource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\Tree\Traits\NestedSetEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Tigris\BaseBundle\Utils\Utils;
use Tigris\CalendarBundle\Repository\BookingCategoryRepository;

#[ORM\Entity(repositoryClass: BookingCategoryRepository::class)]
#[ORM\Table(name: 'calendar_booking_category')]
#[Gedmo\Tree(type: 'nested')]
class BookingCategory implements \Stringable
{
    use NestedSetEntity;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private int|null $id = null;

    #[ORM\Column(length: 120)]
    #[Assert\NotBlank()]
    private string $name;

    #[ORM\Column(length: 7, nullable: true)]
    private string|null $color = null;

    #[ORM\OneToMany(targetEntity: Resource::class, mappedBy: 'category')]
    #[JMS\Exclude]
    private $resources;

    #[ORM\ManyToOne(inversedBy: 'children')]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    #[JMS\Exclude]
    #[Gedmo\TreeParent]
    #[Gedmo\SortableGroup]
    private BookingCategory|null $parent = null;

    #[ORM\OneToMany(targetEntity: BookingCategory::class, mappedBy: 'parent')]
    #[ORM\OrderBy(['left' => 'ASC'])]
    #[JMS\Exclude]
    private Collection $children;

    #[ORM\Column]
    #[Gedmo\SortablePosition]
    private int $position;

    public function __construct()
    {
        $this->left = 0;
        $this->level = 0;
        $this->right = 0;
        $this->root = null;
        $this->children = new ArrayCollection();
    }

    /**
     * Get the value of id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get the value of name.
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set the value of name.
     */
    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of color.
     *
     * @return string
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * Set the value of color.
     */
    public function setColor(string $color): static
    {
        $this->color = $color;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * Get the value of resources.
     */
    public function getResources()
    {
        return $this->resources;
    }

    /**
     * Set the value of resources.
     */
    public function setResources($resources): static
    {
        $this->resources = $resources;

        return $this;
    }

    public function getColorLightness()
    {
        $rgb = Utils::HTMLToRGB($this->color);
        $hsl = Utils::RGBToHSL($rgb);

        return $hsl->lightness;
    }

    /**
     * @return mixed
     */
    public function getRoot()
    {
        return $this->root;
    }

    public function setRoot(mixed $root): static
    {
        $this->root = $root;

        return $this;
    }

    public function getParent(): ?\Tigris\CalendarBundle\Entity\BookingCategory
    {
        return $this->parent;
    }

    public function setParent(mixed $parent): static
    {
        $this->parent = $parent;
        if (is_null($parent)) {
            $this->level = 0;
        }

        return $this;
    }

    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function setChildren(mixed $children): static
    {
        $this->children = $children;

        return $this;
    }

    /**
     * Get the value of position.
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    public function setPosition(int $position): static
    {
        $this->position = $position;

        return $this;
    }

    public function getResourceIds(): array
    {
        $ids = [];
        foreach ($this->resources as $resource) {
            $ids[] = $resource->getId();
        }

        return $ids;
    }
}
