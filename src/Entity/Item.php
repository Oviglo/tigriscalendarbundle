<?php

namespace Tigris\CalendarBundle\Entity;

use App\Entity\Calendar\Event;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Tigris\CalendarBundle\Entity\Model\EventModel;

#[ORM\Entity]
#[ORM\Table(name: 'calendar_item')]
#[ORM\InheritanceType('SINGLE_TABLE')]
#[ORM\DiscriminatorColumn(name: 'type', type: 'string')]
#[ORM\DiscriminatorMap(['event' => Event::class, 'booking' => 'Booking'])]
#[JMS\Discriminator(field: 'mapType', disabled: false, map: ['booking' => Booking::class, 'event' => Event::class, 'eventModel' => EventModel::class])]
abstract class Item
{
    public $type;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    protected ?int $id = null;

    #[ORM\Column(length: 180)]
    protected string $name;

    #[ORM\Column(type: Types::TEXT, nullable: true)]
    protected ?string $description = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    protected \DateTime $startDate;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    protected \DateTime $endDate;

    /**
     * @TODO Delete this field
     */
    #[ORM\Column(type: Types::TIME_MUTABLE, nullable: true)]
    protected ?\DateTime $startTime;

    /**
     * @TODO Delete this field
     */
    #[ORM\Column(type: Types::TIME_MUTABLE, nullable: true)]
    protected $endTime;

    #[ORM\Column]
    protected bool $allDay = false;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTime $cancelDate = null;

    public function __construct()
    {
        $this->startDate = new \DateTime();
        $this->endDate = new \DateTime();
        $this->startTime = new \DateTime();
        $this->endTime = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = strip_tags((string) $description, ['br']);

        return $this;
    }

    #[JMS\VirtualProperty]
    public function getDuration(): int
    {
        $interval = $this->startDate->diff($this->endDate);
        $total = 0;

        $total += (int) $interval->format('%a') * 24 * 60;
        $total += (int) $interval->format('%h') * 60;

        return $total + (int) $interval->format('%i');
    }

    #[JMS\VirtualProperty]
    public function getFormattedDuration(string $format = '%hH%I'): string
    {
        $interval = $this->startDate->diff($this->endDate);

        return $interval->format($format);
    }

    public function setDuration($minutes): self
    {
        $this->endDate = clone $this->startDate;
        $this->endDate->add(new \DateInterval(sprintf('PT%sM', $minutes)));

        return $this;
    }

    public function setStart(\DateTime $start): self
    {
        $this->startDate = $start;

        return $this;
    }

    public function getStartDate(): \DateTime
    {
        return $this->startDate;
    }

    public function setStartDate(\DateTime $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): \DateTime
    {
        return $this->endDate;
    }

    public function setEndDate(\DateTime $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function setEnd(\DateTime $end): self
    {
        $this->endDate = $end;

        return $this;
    }

    public function getStartTime(): ?\DateTime
    {
        return $this->startTime;
    }

    public function setStartTime(?\DateTime $startTime): self
    {
        $this->startDate = clone $this->startDate;
        $this->startDate->setTime($startTime->format('G'), $startTime->format('i'));
        
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): ?\DateTime
    {
        return $this->endTime;
    }

    public function setEndTime(?\DateTime $endTime): self
    {
        $this->endDate = clone $this->endDate;
        $this->endDate->setTime($endTime->format('G'), $endTime->format('i'));
        
        $this->endTime = $endTime;

        return $this;
    }

    public function getStartDateTime(): \DateTime
    {
        return $this->startDate;
    }

    public function getEndDateTime(): \DateTime
    {
        return $this->endDate;
    }

    public function getAllDay(): bool
    {
        return $this->allDay;
    }

    public function setAllDay(bool $allDay): self
    {
        $this->allDay = $allDay;

        return $this;
    }

    public function getCancelDate(): ?\DateTime
    {
        return $this->cancelDate;
    }

    public function setCancelDate(?\DateTime $cancelDate): self
    {
        $this->cancelDate = $cancelDate;

        return $this;
    }

    public function isCanceled(): bool
    {
        return $this->cancelDate instanceof \DateTime;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type): self
    {
        $this->type = $type;

        return $this;
    }
}
