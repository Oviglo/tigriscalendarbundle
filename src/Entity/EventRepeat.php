<?php

namespace Tigris\CalendarBundle\Entity;

use App\Entity\Calendar\Event;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Tigris\CalendarBundle\Entity\Model\EventModel;
use Tigris\CalendarBundle\Repository\EventRepeatRepository;

#[ORM\Entity(repositoryClass: EventRepeatRepository::class)]
#[ORM\Table(name: 'calendar_event_repeat')]
class EventRepeat implements \Stringable
{
    final public const ITERATION_END_NEVER = 0;
    
    final public const ITERATION_END_DATE = 1;
    
    final public const ITERATION_END_COUNT = 2;

    final public const EVERY_DAY = 0;
    
    final public const EVERY_WEEK = 1;
    
    final public const EVERY_MONTH = 2;
    
    final public const EVERY_YEAR = 3;

    final public const EVERY_MONTH_DAY_NUMBER = 0;
    
    final public const EVERY_MONTH_DAY_NAME = 1;

    final public const MONDAY = 0;
    
    final public const TUESDAY = 1;
    
    final public const WEDNESDAY = 2;
    
    final public const THURSDAY = 3;
    
    final public const FRIDAY = 4;
    
    final public const SATURDAY = 5;
    
    final public const SUNDAY = 6;

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\OneToOne(targetEntity: Event::class)]
    private EventModel $event;

    #[ORM\OneToOne(targetEntity: Event::class, cascade: ['all'])]
    private ?EventModel $lastEvent = null;

    #[ORM\OneToMany(targetEntity: Event::class, mappedBy: 'repeat')]
    private Collection $events;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?\DateTime $endDate = null;

    #[ORM\Column]
    private int $iterations = 1;

    #[ORM\Column]
    private int $iterationType = self::ITERATION_END_NEVER;

    #[ORM\Column]
    private int $everyCount = 1;

    #[ORM\Column]
    private int $everyType = self::EVERY_DAY;

    #[ORM\Column]
    private int $everyMonthDayType = self::EVERY_MONTH_DAY_NUMBER;

    #[ORM\Column(type: Types::SIMPLE_ARRAY, nullable: true)]
    private array $days = [];

    public function __construct()
    {
        $this->events = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->event->getName();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEvent(): EventModel
    {
        return $this->event;
    }

    public function setEvent(EventModel $event): static
    {
        $this->event = $event;
        $this->addEvent($event);

        return $this;
    }

    public function getLastEvent(): ?EventModel
    {
        return $this->lastEvent;
    }

    public function setLastEvent(EventModel $lastEvent): static
    {
        $this->lastEvent = $lastEvent;

        return $this;
    }

    public function getEndDate(): ?\DateTime
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTime $endDate = null): static
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getIterations(): int
    {
        return $this->iterations;
    }

    public function setIterations(?int $iterations = null): static
    {
        $this->iterations = $iterations;

        return $this;
    }

    public function getIterationType(): int
    {
        return $this->iterationType;
    }

    public function setIterationType(?int $iterationType = null): static
    {
        $this->iterationType = $iterationType;

        return $this;
    }

    public function getEveryCount(): int
    {
        return $this->everyCount;
    }

    public function setEveryCount(?int $everyCount = null): static
    {
        $this->everyCount = $everyCount;

        return $this;
    }

    public function getEveryType(): int
    {
        return $this->everyType;
    }

    public function setEveryType(?int $everyType = null): static
    {
        $this->everyType = $everyType;

        return $this;
    }

    public function getDays(): array
    {
        return $this->days;
    }

    public function setDays(array $days): static
    {
        $this->days = $days;

        return $this;
    }

    public function getEveryMonthDayType(): int
    {
        return $this->everyMonthDayType;
    }

    public function setEveryMonthDayType(?int $everyMonthDayType = null): static
    {
        $this->everyMonthDayType = $everyMonthDayType;

        return $this;
    }

    public function getNextDateTime(): ?\DateTime
    {
        if (null == $this->lastEvent) {
            return null;
        }

        // If iteration count
        if (self::ITERATION_END_COUNT == $this->iterationType && count($this->events) >= $this->iterations) {
            return null;
        }
        
        $lastDateTime = clone $this->lastEvent->getStartDateTime();

        $lastTime = [$lastDateTime->format('H'), $lastDateTime->format('i')];

        switch ($this->everyType) {
            case self::EVERY_DAY:
                $lastDateTime->add(new \DateInterval(sprintf('P%dD', $this->everyCount)));

                break;

            case self::EVERY_WEEK:
                $dayNumber = (int) $lastDateTime->format('N') - 1;

                $dayKey = array_search($dayNumber, $this->days, true) + 1;
                if ($dayKey >= count($this->days)) {
                    $dayKey = 0;
                }
                
                if (isset($this->days[$dayKey])) {
                    $newDay = $this->days[$dayKey];
                    $lastDateTime->modify('next '.self::getDayName($newDay)); // example: next monday
                }

                break;

            case self::EVERY_MONTH:
                if (self::EVERY_MONTH_DAY_NUMBER == $this->everyMonthDayType) {
                    $lastDateTime->add(new \DateInterval(sprintf('P%dM', $this->everyCount)));
                } elseif (self::EVERY_MONTH_DAY_NAME == $this->everyMonthDayType) { // example: last monday of the month
                    $dayName = self::getDayName((int) $lastDateTime->format('N') - 1);
                    $lastDate = new \DateTime($lastDateTime->format('Y-m-d 0:0:0'));
                    $firstDay = (clone $lastDate)->modify('first '.$dayName.' of this month');
                    if ($lastDate == $firstDay) {
                        $lastDateTime->modify('first '.$dayName.' of next month');

                        break;
                    }

                    $lastDay = clone $lastDate->modify('last '.$dayName);
                    if ($lastDate == $lastDay) {
                        $lastDateTime->modify('last '.$dayName.' of next month');

                        break;
                    }

                    $secondDay = clone $lastDate->modify('second '.$dayName);
                    if ($lastDate == $secondDay) {
                        $lastDateTime->modify('second '.$dayName.' of next month');

                        break;
                    }

                    $thirdDay = clone $lastDate->modify('third '.$dayName);
                    if ($lastDate == $thirdDay) {
                        $lastDateTime->modify('third '.$dayName.' of next month');

                        break;
                    }

                    $fourthDay = clone $lastDate->modify('fourth '.$dayName);
                    if ($lastDate == $fourthDay) {
                        $lastDateTime->modify('fourth '.$dayName.' of next month');

                        break;
                    }
                }

                break;

            case self::EVERY_YEAR:
                $lastDateTime->add(new \DateInterval(sprintf('P%dY', $this->everyCount)));

                break;
        }

        // If iteration date end
        if ($lastDateTime > $this->endDate && self::ITERATION_END_DATE == $this->iterationType) {
            return null;
        }

        $lastDateTime->setTime($lastTime[0], $lastTime[1]);

        return $lastDateTime;
    }

    public function getNextEvent(): ?EventModel
    {
        $nextDateTime = $this->getNextDateTime();

        if (null == $nextDateTime) {
            return null;
        }

        $nextEvent = clone $this->lastEvent;

        $nextEvent->setStartDate($nextDateTime);

        // Update end date
        $nextEndDate = $this->lastEvent->getEndDateTime();
        $dayCountBetweenStartAndEnd = ($nextEndDate->diff($nextDateTime)->format('%r%a') + 1);
        $lastTime = [$nextEndDate->format('H'), $nextEndDate->format('i')];
        $nextEndDate->modify(sprintf('+ %s days', $dayCountBetweenStartAndEnd)); // Add days differences
        $nextEndDate->setTime($lastTime[0], $lastTime[1]);
         // Set time
        $nextEvent->setEndDate($nextEndDate);

        return $nextEvent;
    }

    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function setEvents(Collection $events): static
    {
        $this->events = $events;

        return $this;
    }

    public function addEvent(EventModel $event): void
    {
        $events = [];
        if (!$this->events->contains($event)) {
            $events[] = $event;
            if (!$this->lastEvent instanceof EventModel || $this->lastEvent->getStartDateTime() < $event->getStartDateTime()) {
                $this->lastEvent = $event;
            }
        }
    }

    public static function getDayName(int $num): string
    {
        $dn = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

        return $dn[$num];
    }
}
