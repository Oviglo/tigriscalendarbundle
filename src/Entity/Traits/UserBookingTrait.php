<?php

namespace Tigris\CalendarBundle\Entity\Traits;

use App\Entity\Calendar\Resource;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Tigris\CalendarBundle\Entity\Booking;

trait UserBookingTrait
{
    #[ORM\OneToMany(targetEntity: Booking::class, mappedBy: 'user', cascade: ['persist', 'remove'])]
    #[ORM\JoinColumn(onDelete: 'CASCADE')]
    #[JMS\Exclude]
    private Collection $bookings;

    #[ORM\ManyToOne(targetEntity: Resource::class)]
    #[ORM\JoinColumn(onDelete: 'SET NULL')]
    private ?Resource $defaultResource;

    public function getBookings(): Collection
    {
        return $this->bookings;
    }

    public function setBookings(Collection $bookings): self
    {
        $this->bookings = $bookings;

        return $this;
    }

    public function getDefaultResource(): ?Resource
    {
        return $this->defaultResource;
    }

    public function setDefaultResource(?Resource $defaultResource): self
    {
        $this->defaultResource = $defaultResource;

        return $this;
    }
}
