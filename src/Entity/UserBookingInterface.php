<?php

namespace Tigris\CalendarBundle\Entity;

use App\Entity\Calendar\Resource;
use Doctrine\Common\Collections\Collection;

interface UserBookingInterface
{
    public function getBookings(): Collection;

    public function setBookings(Collection $bookings): self;

    public function getDefaultResource(): null|Resource;

    public function setDefaultResource(null|Resource $defaultResource): self;
}
