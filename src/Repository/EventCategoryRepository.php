<?php

namespace Tigris\CalendarBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\CalendarBundle\Entity\EventCategory;

class EventCategoryRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventCategory::class);
    }
}
