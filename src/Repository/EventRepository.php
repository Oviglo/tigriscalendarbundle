<?php

namespace Tigris\CalendarBundle\Repository;

use App\Entity\Calendar\Event;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Traits\RepositoryTrait;

/**
 * @author Loïc Ovigne <ovigne.loic@gmail.com>
 */
class EventRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    public function findData($criteria): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->addSelect('c')
            ->leftJoin('e.categories', 'c')
        ;

        if (isset($criteria['start']) && isset($criteria['end'])) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX('e.startDate >= :startDate', 'e.startDate < :endDate'),
                    $queryBuilder->expr()->andX('e.endDate < :endDate', 'e.endDate >= :startDate')
                )
            )
            ->setParameter(':startDate', $criteria['start'])
            ->setParameter(':endDate', $criteria['end'])
            ;

            unset($criteria['start']);
            unset($criteria['end']);
        }

        if (isset($criteria['user'])) {
            $queryBuilder->andWhere('e.user = :user')
                ->setParameter(':user', $criteria['user'])
            ;

            unset($criteria['user']);
        }

        if (isset($criteria['categories'])) {
            $queryBuilder->andWhere('c.slug IN (:categories)')
                ->setParameter(':categories', $criteria['categories'])
            ;
        }

        if (isset($criteria['excludeCategories'])) {
            $queryBuilder->andWhere('c.slug NOT IN (:excludeCategories)')
                ->setParameter(':excludeCategories', $criteria['excludeCategories'])
            ;
        }

        $this->addBasicCriteria($queryBuilder, $criteria);

        return new Paginator($queryBuilder, true);
    }

    public function findNext(?int $count = 5): mixed
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->where('e.startDate > :now OR (e.startDate < :now AND e.endDate > :now)')
            ->setParameter(':now', new \DateTime())
            ->orderBy('e.startDate', 'ASC')
            ->setMaxResults($count)
        ;

        return $queryBuilder->getQuery()->getResult();
    }
}
