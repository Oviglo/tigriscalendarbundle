<?php

namespace Tigris\CalendarBundle\Repository;

use App\Entity\Calendar\Resource;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Traits\RepositoryTrait;

/**
 * @author Loïc Ovigne <ovigne.loic@gmail.com>
 */
class ResourceRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Resource::class);
    }

    public function findData($criteria): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('e');
        $queryBuilder->addSelect('c')
            ->join('e.category', 'c')
        ;

        if (isset($criteria['order']) && 'category' == $criteria['order']) {
            $queryBuilder->orderBy('c.name', (($criteria['desc']) ? 'DESC' : 'ASC'));
            unset($criteria['order']);
        }

        $this->addBasicCriteria($queryBuilder, $criteria);

        return new Paginator($queryBuilder, true);
    }
}
