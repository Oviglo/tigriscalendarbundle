<?php

namespace Tigris\CalendarBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepositoryInterface;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Gedmo\Tree\Entity\Repository\NestedTreeRepository;
use Symfony\Bundle\SecurityBundle\Security;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\CalendarBundle\Entity\BookingCategory;
use Tigris\CalendarBundle\Entity\Model\ResourceModel;

/**
 * @author Loïc Ovigne <ovigne.loic@gmail.com>
 */
class BookingCategoryRepository extends NestedTreeRepository implements ServiceEntityRepositoryInterface
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry, private readonly Security $security)
    {
        $manager = $registry->getManagerForClass(BookingCategory::class);

        parent::__construct($manager, $manager->getClassMetadata(BookingCategory::class));
    }

    public function findData(array $criteria): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('e');
        $criteria['order'] = 'left';
        $criteria['rev'] = false;

        $this->addBasicCriteria($queryBuilder, $criteria);

        return new Paginator($queryBuilder, true);
    }

    public function findBooking(): array
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->addSelect('r')
            ->leftJoin('e.resources', 'r')
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    public function findParentForUser(?User $user = null): array
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->addSelect('r, c, i, cr, ci')
            ->leftJoin('e.resources', 'r')
            ->leftJoin('e.children', 'c')
            ->leftJoin('r.image', 'i')
            ->leftJoin('c.resources', 'cr')
            ->leftJoin('cr.image', 'ci')
            ->where('e.parent IS NULL')
            ->orderBy('e.left', 'ASC')
            ->addOrderBy('e.position', 'ASC')
        ;

        /*if (null != $user) {
            if (!$this->security->isGranted('ROLE_ADMIN')) {
                $queryBuilder
                    ->andWhere('r.visibility = :visibilityAll')
                    ->setParameter(':visibilityAll', ResourceModel::VISIBILITY_ALL)
                    ->leftJoin('r.selectedUsers', 'su')
                    ->orWhere('(r.visibility = :visibilityUsers AND su = :user)')
                    ->setParameter(':visibilityUsers', ResourceModel::VISIBILITY_SELECTED_USERS)
                    ->setParameter(':user', $user)
                ;
            }
        } else {
            $queryBuilder->andWhere('r.visibility = :visibilityAll')
                ->setParameter(':visibilityAll', ResourceModel::VISIBILITY_ALL);
        }*/

        return $queryBuilder->getQuery()->getResult();
    }

    public function findWithResource(): array
    {
        $queryBuilder = $this->createQueryBuilder('e')
            ->addSelect('r, i')
            ->join('e.resources', 'r')
            ->leftJoin('r.image', 'i')
        ;

        return $queryBuilder->getQuery()->getResult();
    }

    public function getTree(): array
    {
        $queryBuilder = $this->getRootNodesQueryBuilder();
        $queryBuilder
            ->addSelect('r, i')
            ->addOrderBy('node.position', 'ASC')
            ->leftJoin('node.resources', 'r')
            ->leftJoin('r.image', 'i')
        ;

        return $queryBuilder->getQuery()->getResult();
    }
}
