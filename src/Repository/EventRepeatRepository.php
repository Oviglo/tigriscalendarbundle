<?php

namespace Tigris\CalendarBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\CalendarBundle\Entity\EventRepeat;

class EventRepeatRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EventRepeat::class);
    }
}
