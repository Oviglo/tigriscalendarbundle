<?php

namespace Tigris\CalendarBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Tigris\BaseBundle\Entity\Model\User;
use Tigris\BaseBundle\Traits\RepositoryTrait;
use Tigris\CalendarBundle\Entity\Booking;
use Tigris\CalendarBundle\Entity\Model\ResourceModel;

/**
 * @author Loïc Ovigne <ovigne.loic@gmail.com>
 */
class BookingRepository extends ServiceEntityRepository
{
    use RepositoryTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Booking::class);
    }

    public function findData($criteria, bool $groupByResource = false): Paginator
    {
        $queryBuilder = $this->createQueryBuilder('e');

        $queryBuilder->addSelect('c, r, u')
            ->join('e.resource', 'r')
            ->join('r.category', 'c')
            ->join('e.user', 'u')
        ;

        if (isset($criteria['start']) && isset($criteria['end'])) {
            if (!$criteria['start'] instanceof \DateTime) {
                $criteria['start'] = \DateTime::createFromFormat('d/m/Y', $criteria['start']);
            }
            
            if (!$criteria['end'] instanceof \DateTime) {
                $criteria['end'] = \DateTime::createFromFormat('d/m/Y', $criteria['end']);
            }

            $queryBuilder->andWhere(
                $queryBuilder->expr()->orX(
                    $queryBuilder->expr()->andX('e.startDate >= :startDate', 'e.startDate < :endDate'),
                    $queryBuilder->expr()->andX('e.endDate < :endDate', 'e.endDate >= :startDate')
                )
            )
            ->setParameter(':startDate', $criteria['start'])
            ->setParameter(':endDate', $criteria['end'])
            ;

            unset($criteria['start']);
            unset($criteria['end']);
        }

        if (isset($criteria['user']) && $criteria['user'] instanceof User) {
            $queryBuilder->andWhere('e.user = :user')
                ->setParameter(':user', $criteria['user'])
            ;

            unset($criteria['user']);
        }

        if (isset($criteria['userId']) && 0 != $criteria['userId']) {
            $queryBuilder->andWhere('u.id = :userId')
                ->setParameter(':userId', $criteria['userId'])
            ;

            unset($criteria['userId']);
        }

        if (isset($criteria['category'])) {
            $queryBuilder->andWhere('c.id = :categoryId')
                ->setParameter(':categoryId', $criteria['category'])
            ;

            unset($criteria['category']);
        }

        if (!isset($criteria['canceled']) || !$criteria['canceled']) {
            $queryBuilder->andWhere('e.cancelDate IS NULL');
        }

        if (isset($criteria['resources'])) {
            $queryBuilder->andWhere('(r.id IN (:resources))')
                ->setParameter(':resources', $criteria['resources'])
            ;

            unset($criteria['resources']);
        }

        if (isset($criteria['order'])) {
            if ('resource' == $criteria['order']) {
                $queryBuilder->orderBy('r.name', $criteria['desc'] ? 'desc' : 'asc');
                unset($criteria['order']);
            } elseif ('category' == $criteria['order']) {
                $queryBuilder->orderBy('c.name', $criteria['desc'] ? 'desc' : 'asc');
                unset($criteria['order']);
            }
        }

        $this->addBasicCriteria($queryBuilder, $criteria);

        if ($groupByResource) {
            $queryBuilder->groupBy('r');
        }

        return new Paginator($queryBuilder, true);
    }

    /**
     * Test if the booking already exists.
     */
    public function isBooked(Booking $booking): bool
    {
        $query = $this->createQueryBuilder('b')
            ->select('count(b.id)')
            ->where('b.resource = :resource')
            ->andWhere('b.cancelDate IS NULL')
            ->andWhere('b.endDate > :startDate')
            ->andWhere('b.startDate < :endDate')
            ->andWhere('(
                (b.startDate <= :startDate AND b.endDate >= :endDate ) 
                OR (b.startDate >= :startDate AND b.endDate >= :endDate) 
                OR (b.startDate <= :startDate AND b.endDate <= :endDate) 
                OR (b.startDate >= :startDate AND b.endDate <= :endDate))')
            ->setParameter(':startDate', $booking->getStartDate())
            ->setParameter(':endDate', $booking->getEndDate())
            ->setParameter(':resource', $booking->getResource())
            ->getQuery();

        $result = $query->getSingleScalarResult();

        return (int) $result > 0;
    }

    public function findByMonth(int $month = 0, int $year = 0): array
    {
        if (0 == $month) {
            $month = date('m');
        }
        
        if (0 == $year) {
            $year = date('Y');
        }

        $start = new \DateTime($year.'-'.$month.'-01 00:00');
        $end = clone $start;
        $end->add(new \DateInterval('P1M'));

        return $this->createQueryBuilder('b')
            ->leftJoin('b.resource', 'r')
            ->where('b.startDate >= :startDate')
            ->andWhere('b.startDate < :endDate')
            ->andWhere('r.visibility = :visibility')
            ->andWhere('r.status = :status')
            ->andWhere('b.cancelDate IS NULL')
            ->andWhere('b.endDate < :endDate AND b.endDate >= :startDate')
            ->setParameter(':startDate', $start)
            ->setParameter(':endDate', $end)
            ->setParameter(':visibility', ResourceModel::VISIBILITY_ALL)
            ->setParameter(':status', ResourceModel::STATUS_AVAILABLE)
            ->orderBy('b.startDate', 'desc')
            ->getQuery()
            ->getResult();
    }
}
