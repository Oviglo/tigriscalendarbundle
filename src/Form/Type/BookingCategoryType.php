<?php

namespace Tigris\CalendarBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\CalendarBundle\Entity\BookingCategory;

class BookingCategoryType extends AbstractType
{
    public function __construct(private readonly TranslatorInterface $translator)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $translator = $this->translator;
        $builder
            ->add('name', null, [
                'label' => 'name',
            ])
            ->add('color', ChoiceType::class, [
                'label' => 'color',
                'choices' => [
                    'blue' => '#0074DD',
                    'yellow' => '#FFBF00',
                    'red' => '#FF4444',
                    'green' => '#69BD45',
                    'orange' => '#F7941D',
                    'cyan' => '#36D6E7',
                    'violet' => '#AA66CC',
                    'pink' => '#f18fb8',
                    'indigo' => '#25538C',
                    'apple' => '#B7D466',
                    'brown' => '#9A6600',
                    'gray' => '#8E8E93',
                    'purple' => '#9B51E0',
                    'pine' => '#07796F',
                    'iron' => '#48494B',
                    'chocolat' => '#2B1700',
                    'burgundy' => '#8D021F',
                    'ecru' => '#CEB180',
                    'jungle' => '#29AB87',
                    'mauve' => '#784B84',
                ],
                'choice_attr' => fn ($choice, $key, $value): array => ['data-content' => '<span style="background-color:'.$value.';" class="color-icon"></span> '.$translator->trans($key)],
            ])
        ;

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event): void {
                $category = $event->getData();

                $event->getForm()->add('parent', EntityType::class, [
                    'class' => BookingCategory::class,
                    'multiple' => false,
                    'required' => false,
                    'label' => 'calendar.category.parent',
                    'empty_data' => null,
                    'query_builder' => function ($er) use ($category) {
                        $er = $er->createQueryBuilder('c');
                        if (is_object($category) && $category->getId()) {
                            $er = $er->andWhere('c.id != '.$category->getId())
                            ->andWhere('c.parent IS NULL');
                        }

                        return $er->orderBy('c.left', 'ASC');
                    },
                    'help' => 'calendar.category.parent_help',
                ]);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => BookingCategory::class,
        ]);
    }
}
