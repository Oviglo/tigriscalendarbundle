<?php

namespace Tigris\CalendarBundle\Form\Type;

use App\Entity\Calendar\Resource;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Form\Type\TableCollectionType;
use Tigris\BaseBundle\Form\Type\UploadFileType;
use Tigris\CalendarBundle\Entity\BookingCategory;

class ResourceType extends AbstractType
{
    public function __construct(private readonly array $configs)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $statusLabels = array_map(fn ($value): string => 'calendar.resource.status.'.$value, array_keys(Resource::getStatusList()));
        $statusList = array_combine($statusLabels, array_keys(Resource::getStatusList()));

        $builder
            ->add('name', null, [
                'label' => 'name',
            ])

            ->add('category', EntityType::class, [
                'label' => 'calendar.resource.category',
                'class' => BookingCategory::class,
                'choice_attr' => fn ($choice, $key, $value): array => [
                    'data-content' => '<span style="background-color:'.$choice->getColor().';" class="color-icon"></span> '.$choice->getName(),
                ],
            ])

            ->add('image', UploadFileType::class, [
                'label' => 'calendar.resource.image',
            ])

            ->add('description', TextareaType::class, [
                'label' => 'calendar.resource.description',
                'attr' => [
                    'rows' => 5,
                ],
            ])

            ->add('sendNotification', CheckboxType::class, [
                'label' => 'calendar.resource.send_notification',
            ])

        ;

        if ($this->configs['allow_user_booking']) {
            $builder
                ->add('maxDuration', NumberType::class, [
                    'label' => 'calendar.resource.max_duration',
                    'help' => 'calendar.resource.max_duration_help',
                    'attr' => [
                        'min' => 0,
                    ],
                ])
                ->add('minDuration', NumberType::class, [
                    'label' => 'calendar.resource.min_duration',
                    'help' => 'calendar.resource.min_duration_help',
                    'attr' => [
                        'min' => 0.5,
                    ],
                ])
                ->add('severalDays', CheckboxType::class, [
                    'label' => 'calendar.resource.several_days',
                    'required' => false,
                ])
                ->add('visibility', ChoiceType::class, [
                    'label' => 'calendar.resource.visibility.label',
                    'choices' => [
                        'calendar.resource.visibility.all' => Resource::VISIBILITY_ALL,
                        'calendar.resource.visibility.admins' => Resource::VISIBILITY_ADMINS,
                        'calendar.resource.visibility.selected_users' => Resource::VISIBILITY_SELECTED_USERS,
                    ],
                ])

                ->add('selectedUsers', EntityType::class, [
                    'label' => 'calendar.resource.selected_users',
                    'class' => User::class,
                    'multiple' => true,
                    'required' => false,
                ])

                ->add('lockTimes', TableCollectionType::class, [
                    'label' => 'calendar.resource_lock.label',
                    'entry_type' => ResourceLockTimeType::class,
                    'prototype' => true,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'by_reference' => false,
                    'add_button_text' => 'button.add',
                    'delete_button_text' => 'button.delete',
                    'attr' => [
                        'class' => 'resource-lock-time-form',
                    ],
                ])
                ->add('status', ChoiceType::class, [
                    'label' => 'calendar.resource.status.label',
                    'choices' => $statusList,
                ])

                ->add('statusMessage', TextType::class, [
                    'label' => 'calendar.resource.status.message',
                    'required' => false,
                    'help' => 'calendar.resource.status.message_help',
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Resource::class,
        ]);
    }
}
