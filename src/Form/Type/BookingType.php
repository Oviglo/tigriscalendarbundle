<?php

namespace Tigris\CalendarBundle\Form\Type;

use Doctrine\ORM\QueryBuilder;
use App\Entity\Calendar\Resource;
use App\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\CalendarBundle\Entity\Booking;
use Symfony\Bundle\SecurityBundle\Security;

class BookingType extends AbstractType
{
    public function __construct(private readonly Security $security, private readonly array $configs)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('description', null, [
                'label' => 'calendar.booking.description',
                'required' => false,
            ])

            ->add('startDate', DateTimeType::class, [
                'label' => 'calendar.booking.start',
                'required' => true,
                'widget' => null,
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
            ])
        ;

        if (($this->security->isGranted('ROLE_BOOKING_USER') || $this->security->isGranted('ROLE_ADMIN')) && $this->configs['allow_user_booking']) {
            $builder->add('user', EntityType::class, [
                'label' => 'calendar.booking.book_for',
                'class' => User::class,
                'query_builder' => fn (EntityRepository $er): QueryBuilder => $er->createQueryBuilder('u')
                    ->orderBy('u.username', 'ASC'),
            ]);
        }

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event): void {
            $entity = $event->getData();
            $resource = $entity->getResource();
            $form = $event->getForm();

            $this->addBookingTimeField($form, $resource);
        });
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Booking::class,
        ]);
    }

    private function addBookingTimeField(FormInterface $form, Resource $resource): void
    {
        if ($resource->getSeveralDays()) {
            $form->add('endDate', DateTimeType::class, [
                'label' => 'calendar.booking.end',
                'required' => true,
                'widget' => null,
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
            ]);
        } else {
            $choices = [];
            $resourceMaxTime = ($resource->getMaxDuration() ?? 0) * 60;
            $resourceMinTime = ($resource->getMinDuration() ?? 0.5) * 60;
            $maxTime = ($resourceMaxTime >= 30) ? $resourceMaxTime : 480;

            for ($t = $resourceMinTime; $t <= $maxTime; $t += 30) {
                $hours = str_pad(floor($t / 60), 2, 0, STR_PAD_LEFT);
                $minutes = str_pad($t % 60, 2, 0, STR_PAD_LEFT);
                $str = sprintf('%sH%s', $hours, $minutes);
                $choices[$str] = $t;
            }

            $form->add('duration', ChoiceType::class, [
                'label' => 'calendar.booking.duration.label',
                'choices' => $choices,
            ]);
        }
    }
}
