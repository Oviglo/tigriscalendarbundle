<?php

namespace Tigris\CalendarBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\CalendarBundle\Entity\EventRepeat;

class EventRepeatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('iterations', NumberType::class, [
                'label' => false,
            ])
            ->add('everyCount', NumberType::class, [
                'label' => 'calendar.repeat.iterations',
                'required' => false,
            ])

            ->add('everyType', ChoiceType::class, [
                'label' => false,
                'choices' => [
                    'calendar.repeat.every.days' => EventRepeat::EVERY_DAY,
                    'calendar.repeat.every.weeks' => EventRepeat::EVERY_WEEK,
                    'calendar.repeat.every.months' => EventRepeat::EVERY_MONTH,
                    'calendar.repeat.every.years' => EventRepeat::EVERY_YEAR,
                ],
                'required' => false,
            ])

            ->add('iterationType', ChoiceType::class, [
                'label' => 'calendar.repeat.iteration_type',
                'choices' => [
                    'calendar.repeat.iteration.never' => EventRepeat::ITERATION_END_NEVER,
                    'calendar.repeat.iteration.date' => EventRepeat::ITERATION_END_DATE,
                    'calendar.repeat.iteration.count' => EventRepeat::ITERATION_END_COUNT,
                ],
                'expanded' => true,
            ])

            ->add('days', ChoiceType::class, [
                'label' => 'calendar.repeat.days',
                'choices' => [
                    'monday' => EventRepeat::MONDAY,
                    'tuesday' => EventRepeat::TUESDAY,
                    'wednesday' => EventRepeat::WEDNESDAY,
                    'thursday' => EventRepeat::THURSDAY,
                    'friday' => EventRepeat::FRIDAY,
                    'saturday' => EventRepeat::SATURDAY,
                    'sunday' => EventRepeat::SUNDAY,
                ],
                'multiple' => true,
                'required' => false,
            ])

            ->add('endDate', DateTimeType::class, [
                'label' => false,
                'required' => false,
            ])

            ->add('everyMonthDayType', ChoiceType::class, [
                'label' => 'calendar.repeat.month_day_type',
                'help' => 'calendar.repeat.month_day_type_help',
                'choices' => [
                    'calendar.repeat.month_day.number' => EventRepeat::EVERY_MONTH_DAY_NUMBER,
                    'calendar.repeat.month_day.name' => EventRepeat::EVERY_MONTH_DAY_NAME,
                ],
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EventRepeat::class,
        ]);
    }
}
