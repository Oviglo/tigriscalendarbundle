<?php

namespace Tigris\CalendarBundle\Form\Type;

use App\Entity\Calendar\Event;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\BaseBundle\Form\Type\WysiwygType;
use Tigris\BaseBundle\Validator\Constraints\DateRange;
use Tigris\CalendarBundle\Entity\EventCategory;

class EventType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => 'name',
            ])

            ->add('categories', EntityType::class, [
                'label' => 'calendar.event.categories',
                'required' => false,
                'multiple' => true,
                'class' => EventCategory::class,
            ])

            ->add('startDate', DateTimeType::class, [
                'label' => 'calendar.booking.start',
                'required' => true,
                'widget' => null,
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
            ])

            ->add('endDate', DateTimeType::class, [
                'label' => 'calendar.booking.end',
                'required' => true,
                'widget' => null,
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
                'constraints' => [
                    new DateRange([
                        'startField' => 'startDate',
                        'endField' => 'endDate',
                    ]),
                ],
            ])

            ->add('description', WysiwygType::class, [
                'label' => 'calendar.event.description',
                'tools' => 'maxi',
                'required' => false,
            ])

            ->add('repeat', EventRepeatType::class, [
                'label' => false,
            ])

            ->add('activeRepeat', CheckboxType::class, [
                'label' => 'calendar.event.active_repeat',
                'required' => false,
            ])

            ->add('allDay', CheckboxType::class, [
                'label' => 'calendar.allDay',
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
        ]);
    }
}
