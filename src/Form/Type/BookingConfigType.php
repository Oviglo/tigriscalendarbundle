<?php

namespace Tigris\CalendarBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Tigris\BaseBundle\Validator\Constraints\DateRange;

class BookingConfigType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('booking_start', DateType::class, [
                'label' => 'calendar.booking.config.start',
                'help' => 'calendar.booking.config.start_help',
                'required' => false,
            ])

            ->add('booking_end', DateType::class, [
                'label' => 'calendar.booking.config.end',
                'help' => 'calendar.booking.config.end_help',
                'required' => false,
                /*'constraints' => [
                    new DateRange([
                        'startField' => 'booking_start',
                        'endField' => 'booking_end',
                    ]),
                ],*/
            ])

            ->add('booking_min_time', TimeType::class, [
                'label' => 'calendar.booking.config.min_time',
            ])

            ->add('booking_max_time', TimeType::class, [
                'label' => 'calendar.booking.config.max_time',
            ])

            ->add('booking_visibility', ChoiceType::class, [
                'label' => 'calendar.booking.config.visibility_time',
                'help' => 'calendar.booking.config.visibility_time_help',
                'choices' => [
                    'calendar.booking.config.1week' => '7D',
                    'calendar.booking.config.2weeks' => '14D',
                    'calendar.booking.config.3weeks' => '21D',
                    'calendar.booking.config.4weeks' => '28D',
                ],
                'required' => false,
            ])

            ->add('booking_delete_delay', NumberType::class, [
                'label' => 'calendar.booking.config.delete_delay',
                'required' => false,
            ])

            ->add('booking_delay', NumberType::class, [
                'label' => 'calendar.booking.config.booking_delay',
                'required' => false,
            ])

            ->add('booking_active_days', ChoiceType::class, [
                'label' => 'calendar.booking.config.active_days',
                'multiple' => true,
                'expanded' => true,
                'choices' => [
                    'calendar.booking.config.monday' => 1,
                    'calendar.booking.config.tuesday' => 2,
                    'calendar.booking.config.wednesday' => 3,
                    'calendar.booking.config.thursday' => 4,
                    'calendar.booking.config.friday' => 5,
                    'calendar.booking.config.saturday' => 6,
                    'calendar.booking.config.sunday' => 0,
                ],
                'required' => false,
            ])

            /*->add('show_all_resources', CheckboxType::class, [
                'label' => 'calendar.booking.config.show_all_resources',
                'help' => 'calendar.booking.config.show_all_resources_help',
                'required' => false,
            ])*/
        ;
    }
}
