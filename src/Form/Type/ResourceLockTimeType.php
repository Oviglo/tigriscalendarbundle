<?php

namespace Tigris\CalendarBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Tigris\CalendarBundle\Entity\ResourceLockTime;

class ResourceLockTimeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('days', ChoiceType::class, [
                'label' => 'calendar.resource_lock.days',
                'multiple' => true,
                'expanded' => true,
                'choices' => [
                    'mon' => 1,
                    'tues' => 2,
                    'wednes' => 3,
                    'thurs' => 4,
                    'fri' => 5,
                    'satur' => 6,
                    'sun' => 7,
                ],
            ])

            ->add('startTime', TimeType::class, [
                'label' => 'calendar.resource_lock.start',
                'widget' => 'single_text',
            ])

            ->add('endTime', TimeType::class, [
                'label' => 'calendar.resource_lock.end',
                'widget' => 'single_text',
            ])

            ->add('description', TextareaType::class, [
                'label' => 'calendar.resource_lock.description',
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ResourceLockTime::class,
        ]);
    }

    public function getName(): string
    {
        return 'dtamb_bookingbundle_resource_lock_time';
    }
}
