<?php

namespace Tigris\CalendarBundle;

use Symfony\Component\Config\Definition\Configurator\DefinitionConfigurator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class TigrisCalendarBundle extends AbstractBundle
{
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $parameters = $container->parameters()
            ->set('tigris_calendar', $config)
        ;

        foreach ($config as $key => $value) {
            $parameters->set('tigris_calendar.' . $key, $value);
        }

        $container->import(__DIR__.'/../config/services.yaml');
    }

    public function configure(DefinitionConfigurator $definition): void
    {
        $definition->rootNode()
            ->children()
                ->booleanNode('enable_booking')->defaultTrue()->end()
                ->booleanNode('allow_user_booking')->defaultFalse()->end()
                ->booleanNode('enable_booking_resources')->defaultFalse()->end()
                ->booleanNode('enable_events')->defaultFalse()->end()
            ->end()
        ;
    }
}
