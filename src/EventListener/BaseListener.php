<?php

namespace Tigris\CalendarBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tigris\BaseBundle\Event\CronEvent;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\FormEvent;
use Tigris\BaseBundle\Event\RoleEvent;
use Tigris\CalendarBundle\Form\Type\BookingConfigType;
use Tigris\CalendarBundle\Repository\EventRepeatRepository;

class BaseListener implements EventSubscriberInterface
{
    public function __construct(private readonly array $configs, private readonly EventRepeatRepository $eventRepeatRepository, private readonly EntityManagerInterface $entityManager)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::LOAD_CONFIGS => 'onLoadConfigs',
            Events::CRON => 'onCron',
            Events::LOAD_ROLES => 'onLoadRoles',
        ];
    }

    public function onLoadConfigs(FormEvent $event): void
    {
        $form = $event->getForm();
        if ($this->configs['enable_booking'] && $this->configs['allow_user_booking']) {
            $form->add('TigrisCalendarBundle', BookingConfigType::class, [
                'label' => 'config.booking',
            ]);
        }

        $event->setForm($form);
    }

    public function onLoadRoles(RoleEvent $event): void
    {
        $event->addRoles([
            'ROLE_BOOKING_USER',
        ]);
    }

    public function onCron(CronEvent $event): void
    {
        $repeats = $this->eventRepeatRepository->findAll();
        foreach ($repeats as $repeat) {
            $nextEvent = $repeat->getNextEvent();
            $maxDate = new \DateTime();
            $maxDate->add(new \DateInterval('P1Y'));

            if (null == $nextEvent || $nextEvent->getStartDate() > $maxDate) { // Create only in 1 years
                $this->entityManager->remove($repeat);
                continue;
            }

            $repeat->addEvent($nextEvent);
            $this->entityManager->persist($repeat);
        }

        $this->entityManager->flush();
    }
}
