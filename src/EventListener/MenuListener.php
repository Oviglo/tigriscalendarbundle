<?php

namespace Tigris\CalendarBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tigris\BaseBundle\Event\Events;
use Tigris\BaseBundle\Event\MenuEvent;

class MenuListener implements EventSubscriberInterface
{
    public function __construct(private readonly array $configs)
    {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            Events::LOAD_ADMIN_MENU => 'onLoadAdminMenu',
        ];
    }

    public function onloadAdminMenu(MenuEvent $event): void
    {
        $menu = $event->getMenu();

        if ($this->configs['enable_booking']) {
            $parent = $menu->addChild('calendar.menu.booking', [
                'uri' => '#booking',
            ]);

            $parent->addChild('calendar.menu.calendar', [
                'route' => 'tigris_calendar_admin_booking_index',
            ]);
            $parent->addChild('calendar.menu.booking_list', [
                'route' => 'tigris_calendar_admin_booking_list',
            ]);
            if ($this->configs['enable_booking_resources']) {
                $parent->addChild('calendar.menu.resources', [
                    'route' => 'tigris_calendar_admin_resource_index',
                ]);

                $parent->addChild('calendar.menu.categories', [
                    'route' => 'tigris_calendar_admin_bookingcategory_index',
                ]);
            }
        }

        if ($this->configs['enable_events']) {
            $parent = $menu->addChild('calendar.menu.events', [
                'uri' => '#events',
            ]);

            $parent->addChild('calendar.menu.calendar', [
                'route' => 'tigris_calendar_admin_event_index',
            ]);
            $parent->addChild('calendar.menu.categories', [
                'route' => 'tigris_calendar_admin_eventcategory_index',
            ]);
            $parent->addChild('calendar.menu.repeats', [
                'route' => 'tigris_calendar_admin_eventrepeat_index',
            ]);
        }

        /*$parent->addChild('menu.content.elements', array(
            'route' => 'tigris_content_admin_element_index'
        ));*/
    }
}
