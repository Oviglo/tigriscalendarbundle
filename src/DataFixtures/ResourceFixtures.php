<?php

namespace Tigris\CalendarBundle\DataFixtures;

use App\Entity\Calendar\Resource;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Tigris\CalendarBundle\Entity\BookingCategory;
use Tigris\CalendarBundle\Entity\ResourceLockTime;

class ResourceFixtures extends Fixture
{
    public const REFERENCE = 'resource-';

    private function data(): \Generator
    {
        yield [
            'name' => 'Salle 1',
            'description' => 'Salle de réunion 1',
            'status' => Resource::STATUS_AVAILABLE,
            'reference' => '1',
            'category' => '1',
            'maxDuration' => 4,
            'minDuration' => 1,
            'visibility' => Resource::VISIBILITY_ALL,
            'sendNotification' => false,
            'severalDays' => false,
            'lockTimes' => [
                [
                    'startTime' => new \DateTime('2021-01-01 12:00:00'),
                    'endTime' => new \DateTime('2021-01-01 13:00:00'),
                    'days' => [1, 2, 3, 4, 5],
                ],
            ],
        ];

        yield [
            'name' => 'Salle 2',
            'description' => 'Salle de réunion 2',
            'status' => Resource::STATUS_AVAILABLE,
            'reference' => '2',
            'category' => '1',
            'maxDuration' => 4,
            'minDuration' => 0,
            'visibility' => Resource::VISIBILITY_ALL,
            'sendNotification' => false,
            'severalDays' => false,
            'lockTimes' => [
                [
                    'startTime' => new \DateTime('2021-01-01 08:00:00'),
                    'endTime' => new \DateTime('2021-01-01 10:00:00'),
                    'days' => [3, 4],
                ],
            ],
        ];

        yield [
            'name' => 'Salle 3',
            'description' => 'Salle de réunion 3',
            'status' => Resource::STATUS_UNAVAILABLE,
            'reference' => '3',
            'category' => '2',
            'maxDuration' => 4,
            'minDuration' => 0,
            'visibility' => Resource::VISIBILITY_ADMINS,
            'sendNotification' => false,
            'severalDays' => false,
            'lockTimes' => [
            ],
        ];

        yield [
            'name' => 'Salle 4',
            'description' => 'Salle de réunion 4',
            'status' => Resource::STATUS_AVAILABLE,
            'reference' => '4',
            'category' => '2',
            'maxDuration' => 4,
            'minDuration' => 0,
            'visibility' => Resource::VISIBILITY_ADMINS,
            'sendNotification' => false,
            'severalDays' => false,
            'lockTimes' => [
            ],
        ];
        yield [
            'name' => 'Salle 5',
            'description' => 'Salle de réunion 5',
            'status' => Resource::STATUS_AVAILABLE,
            'reference' => '5',
            'category' => '3',
            'maxDuration' => 4,
            'minDuration' => 0,
            'visibility' => Resource::VISIBILITY_ALL,
            'sendNotification' => false,
            'severalDays' => false,
            'lockTimes' => [
                [
                    'startTime' => new \DateTime('2021-01-01 14:00:00'),
                    'endTime' => new \DateTime('2021-01-01 15:00:00'),
                    'days' => [1],
                ],
            ],
        ];

        yield [
            'name' => 'Salle 6',
            'description' => 'Salle de réunion 6',
            'status' => Resource::STATUS_UNAVAILABLE,
            'reference' => '6',
            'category' => '3',
            'maxDuration' => 4,
            'minDuration' => 0,
            'visibility' => Resource::VISIBILITY_ADMINS,
            'sendNotification' => false,
            'severalDays' => false,
            'lockTimes' => [
            ],
        ];
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->data() as $data) {
            $entity = (new Resource())
                ->setName($data['name'])
                ->setDescription($data['description'])
                ->setMaxDuration($data['maxDuration'])
                ->setMinDuration($data['minDuration'])
                ->setVisibility($data['visibility'])
                ->setSendNotification($data['sendNotification'])
                ->setSeveralDays($data['severalDays'])
                ->setStatus($data['status'])
            ;

            foreach ($data['lockTimes'] as $lockTimeData) {
                $lockTime = new ResourceLockTime();
                $lockTime
                    ->setStartTime($lockTimeData['startTime'])
                    ->setEndTime($lockTimeData['endTime'])
                    ->setDays($lockTimeData['days'])
                    ->setResource($entity)
                ;

                $entity->addLockTime($lockTime);
            }

            if ($data['category']) {
                $entity->setCategory($this->getReference(BookingCategoryFixtures::REFERENCE.$data['category'], BookingCategory::class));
            }

            $manager->persist($entity);

            $this->addReference(self::REFERENCE.$data['reference'], $entity);
        }

        $manager->flush();
    }
}
