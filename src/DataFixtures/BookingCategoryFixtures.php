<?php

namespace Tigris\CalendarBundle\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Tigris\CalendarBundle\Entity\BookingCategory;

class BookingCategoryFixtures extends Fixture
{
    public const REFERENCE = 'booking-category-';

    private function data(): \Generator
    {
        yield [
            'name' => 'Groupe 1',
            'color' => '#8338EC',
            'reference' => 'g1',
        ];

        yield [
            'name' => 'Catégorie 1',
            'color' => '#8338EC',
            'reference' => '1',
            'parent' => 'g1',
        ];

        yield [
            'name' => 'Catégorie 2',
            'color' => '#FB5607',
            'reference' => '2',
            'parent' => 'g1',
        ];

        yield [
            'name' => 'Catégorie 3',
            'color' => '#3A86FF',
            'reference' => '3',
            'parent' => 'g1',
        ];

        yield [
            'name' => 'Groupe 2',
            'color' => '#FF006E',
            'reference' => 'g2',
        ];

        yield [
            'name' => 'Catégorie 4',
            'color' => '#FF006E',
            'reference' => '4',
            'parent' => 'g2',
        ];
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->data() as $data) {
            $entity = (new BookingCategory())
                ->setName($data['name'])
                ->setColor($data['color'])
            ;

            if (isset($data['parent'])) {
                $entity->setParent($this->getReference(self::REFERENCE.$data['parent'], BookingCategory::class));
            }

            $manager->persist($entity);

            $this->addReference(self::REFERENCE.$data['reference'], $entity);
        }

        $manager->flush();
    }
}
