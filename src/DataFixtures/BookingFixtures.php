<?php

namespace Tigris\CalendarBundle\DataFixtures;

use App\Entity\Calendar\Resource;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Tigris\BaseBundle\DataFixtures\UserFixtures;
use Tigris\CalendarBundle\Entity\Booking;

class BookingFixtures extends Fixture implements DependentFixtureInterface
{
    private function data(): \Generator
    {
        yield [
            'start' => new \DateTime('2021-01-01 08:00:00'),
            'end' => new \DateTime('2021-01-01 12:00:00'),
            'user' => 'oviglo',
            'resource' => '1',
            'description' => 'Réservation 1',
        ];

        yield [
            'start' => new \DateTime('today 7:00:00'),
            'end' => new \DateTime('today 9:30:00'),
            'user' => 'oviglo',
            'resource' => '1',
            'description' => 'Réservation 2',
        ];

        yield [
            'start' => new \DateTime('first day of this month 08:30:00'),
            'end' => new \DateTime('first day of this month 11:00:00'),
            'user' => 'asterix',
            'resource' => '2',
            'description' => 'Réservation 3',
        ];

        yield [
            'start' => new \DateTime('first day of this month 08:30:00'),
            'end' => new \DateTime('first day of this month 11:00:00'),
            'user' => 'asterix',
            'resource' => '2',
            'description' => 'Réservation 3',
            'cancelDate' => new \DateTime('second tue of this month 12:00:00'),
        ];

        yield [
            'start' => new \DateTime('first day of this month 14:00:00'),
            'end' => new \DateTime('first day of this month 18:00:00'),
            'user' => 'asterix',
            'resource' => '2',
            'description' => 'Réservation 4',
        ];

        yield [
            'start' => new \DateTime('second monday of this month 07:45:00'),
            'end' => new \DateTime('second monday of this month 10:15:00'),
            'user' => 'obelix',
            'resource' => '2',
            'description' => 'Réservation 5',
        ];

        yield [
            'start' => new \DateTime('second monday of this month 12:00:00'),
            'end' => new \DateTime('second monday of this month 15:00:00'),
            'user' => 'obelix',
            'resource' => '3',
            'description' => 'Réservation 6',
        ];

        yield [
            'start' => new \DateTime('first wednesday of this month 08:00:00'),
            'end' => new \DateTime('first wednesday of this month 12:00:00'),
            'user' => 'asterix',
            'resource' => '3',
            'description' => 'Réservation 7',
        ];

        yield [
            'start' => new \DateTime('first wednesday of this month 12:00:00'),
            'end' => new \DateTime('first wednesday of this month 14:30:00'),
            'user' => 'obelix',
            'resource' => '4',
            'description' => 'Réservation 8',
        ];

        yield [
            'start' => new \DateTime('second tuesday of this month 08:30:00'),
            'end' => new \DateTime('second tuesday of this month 11:00:00'),
            'user' => 'asterix',
            'resource' => '4',
            'description' => 'Réservation 9',
        ];
        yield [
            'start' => new \DateTime('third monday of this month 08:00:00'),
            'end' => new \DateTime('third monday of this month 12:30:00'),
            'user' => 'obelix',
            'resource' => '4',
            'description' => 'Réservation 10',
        ];

        yield [
            'start' => new \DateTime('third monday of this month 14:00:00'),
            'end' => new \DateTime('third monday of this month 18:00:00'),
            'user' => 'asterix',
            'resource' => '4',
            'description' => 'Réservation 11',
        ];

        yield [
            'start' => new \DateTime('last day of this month 10:00:00'),
            'end' => new \DateTime('last day of this month 12:30:00'),
            'user' => 'obelix',
            'resource' => '4',
            'description' => 'Réservation 12',
        ];

        yield [
            'start' => new \DateTime('last day of this month 12:30:00'),
            'end' => new \DateTime('last day of this month 18:00:00'),
            'user' => 'asterix',
            'resource' => '5',
            'description' => 'Réservation 13',
        ];

        yield [
            'start' => new \DateTime('+1 month 08:00:00'),
            'end' => new \DateTime('+1 month 12:00:00'),
            'user' => 'oviglo',
            'resource' => '5',
            'description' => 'Réservation 14',
        ];
    }

    public function load(ObjectManager $manager): void
    {
        foreach ($this->data() as $data) {
            $entity = (new Booking())

                ->setUser($this->getReference(UserFixtures::REFERENCE.$data['user'], User::class))
                ->setResource($this->getReference(ResourceFixtures::REFERENCE.$data['resource'], Resource::class))
                ->setDescription($data['description'])
                ->setStart($data['start'])
                ->setEnd($data['end'])
            ;

            if (isset($data['cancelDate'])) {
                $entity->setCancelDate($data['cancelDate']);
            }

            $manager->persist($entity);
        }

        $manager->flush();
    }

    public function getDependencies(): array
    {
        return [
            UserFixtures::class,
            BookingCategoryFixtures::class,
            ResourceFixtures::class,
        ];
    }
}
