import TigrisEvents from '../../../tigris-base-bundle/assets/js/tigris-events'
// ==================== EVENT INTERATIONS

function calendarActiveRepeat(active) {
    let repeatFormDiv = document.querySelector(".repeat-form");
    let repeatFormInputs = document.querySelectorAll(".repeat-form input");
    if (active) {
        repeatFormDiv.classList.add('show');
        for (const field of repeatFormInputs) {
            field.disabled = false;
        }
    } else {
        repeatFormDiv.classList.remove('show');
        for (const field of repeatFormInputs) {
            field.disabled = true;
        }
    }
}

function calendarSelectEvery(value) {
    let days = document.querySelector(".repeat-week");
    let months = document.querySelector(".repeat-month");
    days.style.display = "none";
    months.style.display = "none";
    switch (parseInt(value)) {
        case 1:
            days.style.display = "block";
            break;
        case 2:
            months.style.display = "block";
            break;
    }
}

function calendarSelectIterationType(value) {
    let endDate = document.querySelector("#event_repeat_endDate");
    let iterations = document.querySelector("#event_repeat_iterations");
    endDate.readOnly = true;
    iterations.readOnly = true;
    switch (parseInt(value)) {
        case 1:
            endDate.readOnly = false;
            break;
        case 2:
            iterations.readOnly = false;
            break;

    }
}

function calendarAllDay(value) {
    const timeSelect = document.querySelector("#nav-general .time")

    if (null != timeSelect) {
        timeSelect.style.display = value ? "none" : "block"
    }
}

document.addEventListener(TigrisEvents.EVENT_AJAX_LOADED, () => {
    // All day
    let allDayCheck = document.getElementById("event_allDay");
    if (null != allDayCheck) {
        calendarAllDay(allDayCheck.checked);
        allDayCheck.addEventListener("change", () => {
            calendarAllDay(allDayCheck.checked);
        });
    }

    // ITERATION
    let activeRepeatCheck = document.getElementById("event_activeRepeat");

    if (null === activeRepeatCheck) {
        return;
    }

    calendarActiveRepeat(activeRepeatCheck.checked);

    activeRepeatCheck.addEventListener('change', function () {
        calendarActiveRepeat(activeRepeatCheck.checked);
    });

    // every type
    let everyTypeSelect = document.getElementById("event_repeat_everyType");
    calendarSelectEvery(everyTypeSelect.value);
    everyTypeSelect.addEventListener("change", function () {
        calendarSelectEvery(everyTypeSelect.value);
    });

    // Iterations
    let iterationType = document.querySelectorAll('[name="event[repeat][iterationType]"]');

    iterationType.forEach(function (element) {
        if (element.checked) {
            calendarSelectIterationType(element.value);
        }
        element.addEventListener("change", function () {
            if (element.checked) {
                calendarSelectIterationType(element.value);
            }
        });
    });

});