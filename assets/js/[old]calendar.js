import { Calendar } from '@fullcalendar/core';
import interactionPlugin, { Draggable } from '@fullcalendar/interaction';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import bootstrap5Plugin from '@fullcalendar/bootstrap5';
import interaction from '@fullcalendar/interaction';
import frLocale from '@fullcalendar/core/locales/fr';
import TigrisDialog from '../../../tigris-base-bundle/assets/js/tigris-dialog';
import TigrisEvents from '../../../tigris-base-bundle/assets/js/tigris-events'

//bootstrap5Plugin.themeClasses.bootstrap.prototype.baseIconClass = 'fa' + iconStyle;
document.addEventListener(TigrisEvents.VUE_APP_MOUNTED, () => {
    if (activeDays === undefined) {
        var activeDays = [0, 1, 2, 3, 4, 5, 6];
    }
    let containerEl = document.getElementById('booking-categories');
    let calendarEl = document.querySelector('#calendar:not(.fc)');
    let resources = [];
    let dialog = new TigrisDialog;

    if (null !== calendarEl) {

        let newEventRoute = calendarEl.getAttribute("new-event-route");
        let showItemRoute = "tigris_calendar_booking_show";
        if (calendarEl.hasAttribute('show-item-route')) {
            showItemRoute = calendarEl.getAttribute("show-item-route");
        }

        let listRoute = "tigris_calendar_booking_list";
        if (calendarEl.hasAttribute('list-route')) {
            listRoute = calendarEl.getAttribute("list-route");
        }
        let initView = calendarEl.getAttribute("init-view");
        if (null == initView) {
            initView = 'dayGridMonth';
        }
        let allDaySlot = calendarEl.getAttribute("all-day-slot") == true;

        const calendar = new Calendar(calendarEl, {
            initialView: initView,
            locale: frLocale,
            plugins: [dayGridPlugin, timeGridPlugin, bootstrap5Plugin, interactionPlugin, interaction],
            headerToolbar: { start: 'today,prev,next', center: 'title', end: 'dayGridMonth,timeGridWeek,dayGridDay' },
            themeSystem: 'bootstrap5',
            dayCount: 30,
            timeZone: 'UTC',
            aspectRatio: 1.5,
            allDaySlot: allDaySlot,
            droppable: true,
            events: {
                url: Routing.generate(listRoute),
                type: 'GET',
                extraParams: () => {
                    return {
                        resources: resources,
                    };
                }
            },

            eventReceive: (info) => {
                let event = info.event;
                const dateStart = event.start.toUTCString();
                const resourceId = parseInt(info.draggedEl.getAttribute('data-id'));

                dialog.open(Routing.generate('tigris_calendar_booking_new', { id: resourceId, dateStart: dateStart }), (formData, data) => {
                    calendar.refetchEvents();
                    event.remove();
                }, () => {
                    event.remove();
                });
            },
            dateClick: (info) => {
                let dateStart = info.date.toUTCString();
                if (newEventRoute != null) {

                    dialog.open(Routing.generate(newEventRoute, { dateStart: dateStart }), (formData, data) => {

                    }, () => {

                    });
                }
            },

            eventClick: (info) => {
                if (null != showItemRoute) {
                    let id = info.event.id;

                    if (id != 'null') {

                        dialog.open(Routing.generate(showItemRoute, { id: id }), (formData, data) => {

                        }, () => {

                        });
                    }
                }
            },

            businessHours: {
                daysOfWeek: activeDays ?? [0, 1, 2, 3, 4, 5, 6],
                startTime: '00:00',
                endTime: '23:59'
            }
        });

        if (null != containerEl) {
            new Draggable(containerEl, {
                itemSelector: '.btn-drag',
                eventData: (eventEl) => {

                    return {
                        title: eventEl.innerText,
                        duration: '01:00',
                        create: true,
                        backgroundColor: eventEl.style.backgroundColor,
                        borderColor: eventEl.style.backgroundColor,
                    };
                }
            });
        }

        calendar.render()

        // Booking ressources
        // Select category
        const categoryCheckboxes = document.querySelectorAll('[data-select-category]');
        for (const categoryCheckbox of categoryCheckboxes) {
            categoryCheckbox.addEventListener('change', (ev) => {
                ev.preventDefault();
                let checkbox = ev.target;
                const categoryId = checkbox.getAttribute('data-select-category');
                let resourceCheckboxes = document.querySelectorAll('[data-category="' + categoryId + '"]');
                resources = [];
                for (const resourceCheckbox of resourceCheckboxes) {
                    resourceCheckbox.checked = checkbox.checked;
                    let resourceId = parseInt(resourceCheckbox.getAttribute('data-id'));
                    if (resourceCheckbox.checked) {
                        resources.push(resourceId);
                    }
                }

                if (typeof calendar != 'undefined') {
                    calendar.refetchEvents();
                }

            });
        }

        const evt = new CustomEvent('change');
        const checkedCategory = document.querySelector('[data-select-category]:checked');
        if (checkedCategory != null) {
            checkedCategory.dispatchEvent(evt);
        }

        // Select one resource
        let resourceCheckboxes = document.querySelectorAll('.select-resource');
        for (const resourceCheckbox of resourceCheckboxes) {
            resourceCheckbox.addEventListener('change', (ev) => {
                ev.preventDefault();
                let checkbox = ev.target;
                let resourceId = parseInt(checkbox.getAttribute('data-id'));
                if (checkbox.checked) {
                    resources.push(resourceId);
                } else if (resources.indexOf(resourceId) !== -1) {
                    resources.splice(resources.indexOf(resourceId), 1);
                }

                // Check category if all checked
                let parent = checkbox.closest('.card');
                if (parent.querySelector('.select-resource:not(:checked)') == null && parent.querySelector('[data-select-category]') !== null) {
                    parent.querySelector('[data-select-category]').checked = true;
                }
                if (parent.querySelector('.select-resource:checked') == null && parent.querySelector('[data-select-category]') !== null) {
                    parent.querySelector('[data-select-category]').checked = false;
                }

                calendar.refetchEvents();
            });
        }

        const accordionCategory = document.querySelector('#booking-calendar #booking-categories .accordion')
        if (null !== accordionCategory) {
            accordionCategory.addEventListener('show.bs.collapse', () => {
                //     // do something...
            })
        }

    }


});