<?php

namespace Tigris\CalendarBundle\Tests\Application\Controller\Admin;

use PHPUnit\Framework\Attributes\CoversClass;
use Tigris\CalendarBundle\Controller\Admin\ResourceController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Contracts\Translation\TranslatorInterface;
use Tigris\CalendarBundle\Manager\CalendarManager;
use App\Entity\Calendar\Resource;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;

#[CoversClass(ResourceController::class)]
class ResourceControllerTest extends AbstractLoginWebTestCase
{
    public function testIndex()
    {
        $this->loginAdmin();
        $this->client->request('GET', '/admin/calendar/resource/');

        $this->assertResponseIsSuccessful();
    }

    public function testList()
    {
        $this->loginAdmin();
        $this->client->request('GET', '/admin/calendar/resource/list');

        $this->assertResponseIsSuccessful();
        $this->assertJson($this->client->getResponse()->getContent());
    }

    public function testNewResource()
    {
        $this->loginAdmin();
        $crawler = $this->client->request('GET', '/admin/calendar/resource/new');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('resource_actions_save')->form();
        $form['resource[name]'] = 'Test Resource';
        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect('/admin/calendar/resource/'));
    }

    public function testEditResource()
    {
        $this->loginAdmin();
        $crawler = $this->client->request('GET', '/admin/calendar/resource/edit/1');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('resource_actions_save')->form();
        $form['resource[name]'] = 'Updated Resource';
        $this->client->submit($form);

        $this->assertTrue($this->client->getResponse()->isRedirect('/admin/calendar/resource/'));
    }
}
