<?php

namespace Tigris\CalendarBundle\Tests\Application\Controller\Admin;

use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use Symfony\Component\HttpFoundation\Response;
use Tigris\BaseBundle\Tests\AbstractLoginWebTestCase;
use Tigris\CalendarBundle\Controller\Admin\BookingCategoryController;
use Tigris\CalendarBundle\Repository\BookingCategoryRepository;

#[CoversClass(BookingCategoryController::class)]
class BookingCategoryControllerTest extends AbstractLoginWebTestCase
{
    #[Test]
    public function index(): void
    {
        $this->loginAdmin();

        $this->client->request('GET', '/admin/calendar/booking/category/');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function data(): void
    {
        $this->loginAdmin();

        $this->client->request('GET', '/admin/calendar/booking/category/data');

        self::assertResponseIsSuccessful();
    }

    #[Test]
    public function new(): void
    {
        $this->loginAdmin();

        $crawler = $this->client->request('GET', '/admin/calendar/booking/category/new');

        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('booking_category_actions_save')->form();

        $this->client->submit($form, [
            'booking_category[name]' => 'Category test',
            'booking_category[color]' => '#25538C',
        ]);

        self::assertResponseRedirects();

        $entity = self::getContainer()->get(BookingCategoryRepository::class)->findOneBy(['name' => 'Category test']);

        self::assertNotNull($entity);
    }

    #[Test]
    public function newInvalid(): void
    {
        $this->loginAdmin();

        $crawler = $this->client->request('GET', '/admin/calendar/booking/category/new');

        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('booking_category_actions_save')->form();

        $this->client->submit($form, [
            'booking_category[name]' => '',
            'booking_category[color]' => '#25538C',
        ]);

        self::assertResponseStatusCodeSame(Response::HTTP_UNPROCESSABLE_ENTITY);
    }

    #[Test]
    public function edit(): void
    {
        $this->loginAdmin();

        $entity = self::getContainer()->get(BookingCategoryRepository::class)->findOneBy(['name' => 'Catégorie 1']);

        $crawler = $this->client->request('GET', '/admin/calendar/booking/category/edit/'.$entity->getId());

        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('booking_category_actions_save')->form();

        $this->client->submit($form, [
            'booking_category[name]' => 'Category 1 edited',
            'booking_category[color]' => '#25538C',
        ]);

        self::assertResponseRedirects();

        $entity = self::getContainer()->get(BookingCategoryRepository::class)->findOneBy(['name' => 'Category 1 edited']);

        self::assertNotNull($entity);
    }

    #[Test]
    public function delete(): void
    {
        $this->loginAdmin();

        $entity = self::getContainer()->get(BookingCategoryRepository::class)->findOneBy(['name' => 'Catégorie 1']);

        $crawler = $this->client->request('GET', '/admin/calendar/booking/category/remove/'.$entity->getId());

        self::assertResponseIsSuccessful();

        $form = $crawler->selectButton('form_actions_save')->form();

        $this->client->submit($form);

        self::assertResponseRedirects();

        $entity = self::getContainer()->get(BookingCategoryRepository::class)->findOneBy(['name' => 'Catégorie 1']);

        self::assertNull($entity);
    }
}
