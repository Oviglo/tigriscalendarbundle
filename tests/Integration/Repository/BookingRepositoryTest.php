<?php

namespace Tigris\CalendarBundle\Tests\Integration\Repository;

use App\Entity\Calendar\Resource;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Tigris\CalendarBundle\Entity\Booking;
use Tigris\CalendarBundle\Repository\BookingRepository;
use Tigris\CalendarBundle\Repository\ResourceRepository;

/**
 * @internal
 */
#[CoversClass(BookingRepository::class)]
class BookingRepositoryTest extends KernelTestCase
{
    public function testFindData()
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);

        /** @var BookingRepository */
        $repository = $em->getRepository(Booking::class);

        $booking = $repository->findData(['startDate' => new \DateTime('first day of this month'), 'endDate' => new \DateTime('last day of this month')]);

        static::assertGreaterThan(0, $booking->count());
    }

    #[DataProvider('bookingProvider')]
    public function testIsBooked(Booking $booking, bool $expected): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);

        /** @var BookingRepository */
        $repository = $em->getRepository(Booking::class);

        $result = $repository->isBooked($booking);

        static::assertEquals($expected, $result);
    }

    public static function bookingProvider(): \Iterator
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);

        /** @var ResourceRepository */
        $repository = $em->getRepository(Resource::class);

        $salle1 = $repository->findOneBy(['name' => 'Salle 1']);
        $booking = (new Booking())
            ->setResource($salle1)
            ->setStartDate(new \DateTime('first day of this month 12:00:00'))
            ->setEndDate(new \DateTime('first day of this month 13:00:00'))
        ;

        yield [$booking, false];

        $salle2 = $repository->findOneBy(['name' => 'Salle 2']);
        $booking = (new Booking())
            ->setResource($salle2)
            ->setStartDate(new \DateTime('today 7:00:00'))
            ->setEndDate(new \DateTime('today 9:00:00'))
        ;

        yield [$booking, false];

        $booking = (new Booking())
            ->setResource($salle2)
            ->setStartDate(new \DateTime('first day of this month 7:00:00'))
            ->setEndDate(new \DateTime('first day of this month 12:00:00'))
        ;

        yield [$booking, true];

        $booking = (new Booking())
            ->setResource($salle2)
            ->setStartDate(new \DateTime('first saturday of this month 9:30:00'))
            ->setEndDate(new \DateTime('first saturday of this month 12:00:00'))
        ;

        yield [$booking, true];

        $booking = (new Booking())
            ->setResource($salle2)
            ->setStartDate(new \DateTime('second monday of this month 9:30:00'))
            ->setEndDate(new \DateTime('second monday of this month 10:00:00'))
        ;

        yield [$booking, true];

        $booking = (new Booking())
            ->setResource($salle2)
            ->setStartDate(new \DateTime('today 11:00:00'))
            ->setEndDate(new \DateTime('today 12:00:00'))
        ;

        yield [$booking, false];

        $booking = (new Booking())
            ->setResource($salle2)
            ->setStartDate(new \DateTime('last monday of this month 11:00:00'))
            ->setEndDate(new \DateTime('last monday of this month 12:00:00'))
        ;

        yield [$booking, false];

        $booking = (new Booking())
            ->setResource($salle1)
            ->setStartDate(new \DateTime('last day of this month 9:30:00'))
            ->setEndDate(new \DateTime('last day of this month 10:00:00'))
        ;

        yield [$booking, false];
    }

    #[DataProvider('findByMonthProvider')]
    public function testFindByMonth(int $month, int $year, int $expectedCount): void
    {
        /** @var EntityManagerInterface */
        $em = static::getContainer()->get(EntityManagerInterface::class);

        /** @var BookingRepository */
        $repository = $em->getRepository(Booking::class);

        $bookings = $repository->findByMonth($month, $year);

        static::assertCount($expectedCount, $bookings);
    }

    public static function findByMonthProvider(): \Iterator
    {
        yield [(int) date('m'), (int) date('Y'), 5];

        yield [0, 0, 5];

        yield [1, 2000, 0];

        yield [((int) date('m')) + 1, (int) date('Y'), 1];
    }
}
